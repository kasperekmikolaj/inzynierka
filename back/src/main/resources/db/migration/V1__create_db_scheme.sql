BEGIN;

CREATE TABLE "user"
(
    id         BIGSERIAL PRIMARY KEY,
    username   VARCHAR(25)
        CONSTRAINT username_len_cons CHECK (char_length(username) > 3) NOT NULL UNIQUE,
    "email"    VARCHAR(255)
        CONSTRAINT email_len_cons CHECK (char_length(email) > 4)       NOT NULL UNIQUE,
    "password" VARCHAR(60)
        CONSTRAINT password_len_cons CHECK (char_length(password) > 7) NOT NULL,
    telephone  VARCHAR(20)
);

CREATE TABLE "group"
(
    id          BIGSERIAL PRIMARY KEY,
    name        VARCHAR(40)
        CONSTRAINT group_name_len_cons CHECK (char_length(name) > 3) NOT NULL,
    type        VARCHAR(20),
    image       TEXT,
    description TEXT,
    is_public   BOOLEAN                                              NOT NULL
);

CREATE TABLE group_member
(
    id         BIGSERIAL PRIMARY KEY,
    user_id    BIGINT REFERENCES "user" (id) ON DELETE CASCADE  NOT NULL,
    group_id   BIGINT REFERENCES "group" (id) ON DELETE CASCADE NOT NULL,
    user_color VARCHAR(7)                                       NOT NULL,
    user_role  VARCHAR(20)                                      NOT NULL,
    UNIQUE (user_id, group_id)
);

CREATE TABLE item
(
    id          BIGSERIAL PRIMARY KEY,
    "name"      VARCHAR(25)                                      NOT NULL,
    description TEXT,
    group_id    BIGINT REFERENCES "group" (id) ON DELETE CASCADE NOT NULL,
    image       TEXT
);

CREATE TABLE item_reservation
(
    id               BIGSERIAL PRIMARY KEY,
    item_id          BIGINT REFERENCES item (id) ON DELETE CASCADE   NOT NULL,
    reservation_date DATE                                            NOT NULl,
    booking_user_id  BIGINT REFERENCES "user" (id) ON DELETE CASCADE NOT NULL,
    UNIQUE (item_id, reservation_date)
);

CREATE TABLE group_invitation
(
    id         BIGSERIAL PRIMARY KEY,
    user_id    BIGINT REFERENCES "user" (id) ON DELETE CASCADE  NOT NULL,
    group_id   BIGINT REFERENCES "group" (id) ON DELETE CASCADE NOT NULL,
    UNIQUE (user_id, group_id)
);

COMMIT;