package com.papaj.rental.configuration.security;

import com.papaj.rental.configuration.security.properties.JwtBlacklist;
import com.papaj.rental.configuration.security.properties.JwtProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JwtAuthenticationFilter extends BasicAuthenticationFilter {

    private static final String TOKEN_PREFIX = "Bearer ";

    private final JwtProperties jwtProperties;
    private final JwtBlacklist blacklist;

    public JwtAuthenticationFilter(
            AuthenticationManager authenticationManager,
            final JwtProperties jwtProperties,
            JwtBlacklist blacklist) {
        super(authenticationManager);
        this.jwtProperties = jwtProperties;
        this.blacklist = blacklist;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        String header = req.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            this.clearContext(req, res, chain);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        if (authentication == null) {
            this.clearContext(req, res, chain);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    /**
     * Retrieves information about the user from JWT token
     */
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION).replace(TOKEN_PREFIX, "");

        Jws<Claims> parsedToken =
                Jwts.parserBuilder()
                        .setSigningKey(jwtProperties.getLoginKey().getBytes())
                        .build()
                        .parseClaimsJws(token);

        if (blacklist.contains(parsedToken)) {
            return null;
        }

        String email = parsedToken.getBody().getSubject();
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(Role.USER));

        return new UsernamePasswordAuthenticationToken(email, null, authorities);
    }

    private void clearContext(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        SecurityContextHolder.clearContext();
        chain.doFilter(req, res);
    }
}
