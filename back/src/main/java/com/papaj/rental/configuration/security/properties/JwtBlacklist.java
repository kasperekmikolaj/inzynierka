package com.papaj.rental.configuration.security.properties;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class JwtBlacklist {

    private List<Jws<Claims>> list;
    private final JwtProperties jwtProperties;

    public JwtBlacklist(JwtProperties jwtProperties) {
        this.list = new ArrayList<>();
        this.jwtProperties = jwtProperties;
    }

    public void add(Jws<Claims> token) {
        this.list.add(token);
    }

    public boolean contains(Jws<Claims> token) {
        return this.list
                .stream()
                .anyMatch(t -> t.getSignature().equals(token.getSignature()));
    }

    //works every hour (1:05, 2:05...)
    @Scheduled(cron = "5 * * * *")
    private void clearOldTokens() {
        this.list = this.list
                .stream()
                .filter(token -> token.getBody().getExpiration().after(new Date()))
                .collect(Collectors.toList());
    }
}
