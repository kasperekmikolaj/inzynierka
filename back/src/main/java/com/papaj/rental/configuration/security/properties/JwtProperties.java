package com.papaj.rental.configuration.security.properties;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
@ConfigurationProperties(prefix = "jwt")
public class JwtProperties {

  private String loginKey;
  private int loginExpirationTimeMs;

  public void setLoginKey(final String loginKey) {
    this.loginKey = loginKey;
  }

  public void setLoginExpirationTimeMs(final int loginExpirationTimeMs) {
    this.loginExpirationTimeMs = loginExpirationTimeMs;
  }
}
