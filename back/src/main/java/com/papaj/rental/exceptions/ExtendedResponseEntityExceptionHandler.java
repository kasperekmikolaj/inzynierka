package com.papaj.rental.exceptions;


import com.papaj.rental.dto.ErrorDto;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExtendedResponseEntityExceptionHandler {

    @ExceptionHandler(EmptyResultDataAccessException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleEmptyResultDataAccessException(EmptyResultDataAccessException exception) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + exception.getMessage());
        return errorDto;
    }

    @ExceptionHandler(FailedAuthorizationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorDto handleFailedAuthorizationException() {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + "Incorrect mail or password.");
        return errorDto;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleMethodArgumentNotValid(MethodArgumentNotValidException exception) {
        ErrorDto errorDto = new ErrorDto();
        FieldError fieldError = exception.getBindingResult().getFieldError();
        if (fieldError != null) {
            errorDto.setMessage("@ " + "Provided wrong data for field " + fieldError.getField());
        }
        return errorDto;
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorDto handleDataIntegrityViolationException(DataIntegrityViolationException exception) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + exception.getMostSpecificCause().getMessage());
        return errorDto;
    }

    @ExceptionHandler(EmailExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorDto handleEmailExistExceptionException() {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + "Provided email is already in use");
        return errorDto;
    }

    @ExceptionHandler(UsernameExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorDto handleUsernameExistException() {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + "Provided username is already in use");
        return errorDto;
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto handleNotFoundException() {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + "Resource not found :(");
        return errorDto;
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorDto handleUnauthorizedException() {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + "You are not allowed to proceed this action. Login and try again.");
        return errorDto;
    }

    @ExceptionHandler(ExpiredJwtException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorDto handleExpiredJwtException() {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + "Token expired. Try login again.");
        return errorDto;
    }

    @ExceptionHandler(UserAlreadyGroupMemberException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorDto handleUserAlreadyGroupMemberException() {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + "Selected user is already a member of this group.");
        return errorDto;
    }

    @ExceptionHandler(GroupNotPublicException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorDto handleGroupNotPublicException() {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage("@ " + "Can not return members of not public group.");
        return errorDto;
    }
}

