package com.papaj.rental.dto.group_member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupMemberDeleteDto {

    private long userId;
    private long groupId;
}
