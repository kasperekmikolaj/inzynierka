package com.papaj.rental.dto.item;

import com.papaj.rental.models.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ItemDto {

    private long id;
    private String name;
    private String description;
    private String image;

    public ItemDto(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.description = item.getDescription();
        this.image = item.getImage();
    }
}
