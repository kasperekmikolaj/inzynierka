package com.papaj.rental.dto.group_member;

import com.papaj.rental.models.GroupMember;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupMemberDto {

    private Long userId;
    private String username;
    private String userColor;
    private String userRole;
    private String userEmail;

    public GroupMemberDto(GroupMember groupMember) {
        this.userId = groupMember.getUser().getId();
        this.username = groupMember.getUser().getUsername();
        this.userColor = groupMember.getUserColor();
        this.userRole = groupMember.getUserRole().toString();
        this.userEmail = groupMember.getUser().getEmail();
    }
}
