package com.papaj.rental.dto.group_member;

import com.papaj.rental.models.UserRoleEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class GroupMemberPatchRoleDto {

    private long userToChangeId;

    @NotNull
    private long groupId;

    @NotBlank
    @Size(max = 20)
    private UserRoleEnum role;
}
