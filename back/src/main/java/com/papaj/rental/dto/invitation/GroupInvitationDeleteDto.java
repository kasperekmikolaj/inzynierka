package com.papaj.rental.dto.invitation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GroupInvitationDeleteDto {

    private long invitedUserId;
    private long groupId;

}
