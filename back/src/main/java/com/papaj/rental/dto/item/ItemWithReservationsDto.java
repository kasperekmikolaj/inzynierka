package com.papaj.rental.dto.item;

import com.papaj.rental.dto.reservation.ItemReservationDto;
import com.papaj.rental.models.Item;
import com.papaj.rental.models.ItemReservation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ItemWithReservationsDto {

    private long itemId;
    private String name;
    private String description;
    private String image;
    private List<ItemReservationDto> reservations;

    public ItemWithReservationsDto(Item item, List<ItemReservationDto> reservations) {
        this.itemId = item.getId();
        this.name = item.getName();
        this.description = item.getDescription();
        this.image = item.getImage();
        this.reservations = reservations;
    }
}
