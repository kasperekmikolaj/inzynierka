package com.papaj.rental.dto.invitation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GroupInvitationDto {

    private String invitedUserEmail;
    private long groupId;

}
