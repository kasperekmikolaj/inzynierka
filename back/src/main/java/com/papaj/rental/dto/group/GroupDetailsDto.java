package com.papaj.rental.dto.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.papaj.rental.dto.group_member.GroupMemberDto;
import com.papaj.rental.dto.item.ItemDto;
import com.papaj.rental.models.Group;
import com.papaj.rental.models.GroupMember;
import com.papaj.rental.models.Item;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GroupDetailsDto {

    private long id;
    private String name;
    private String type;
    private String image;
    private String description;
    @JsonProperty(value = "isPublic")
    private boolean isPublic;
    @JsonProperty(value = "isUserGroupMember")
    private boolean isUserGroupMember;
    private String currentUserRole;
    private String currentUserColor;
    private List<ItemDto> items;
    private List<GroupMemberDto> members;

    public GroupDetailsDto(Group group, String currentUserRole, String currentUserColor, boolean isUserGroupMember) {
        this.id = group.getId();
        this.name = group.getName();
        this.type = group.getType();
        this.image = group.getImage();
        this.description = group.getDescription();
        this.isPublic = group.getIsPublic();
        this.isUserGroupMember = isUserGroupMember;
        this.currentUserRole = currentUserRole;
        this.currentUserColor = currentUserColor;

        this.items = new ArrayList<>();
        for (Item item : group.getItems()) {
            this.items.add(new ItemDto(item));
        }
        if (isUserGroupMember) {
            this.members = new ArrayList<>();
            for (GroupMember member : group.getMembers()) {
                this.members.add(new GroupMemberDto(member));
            }
        } else {
            this.members = null;
        }
    }
}
