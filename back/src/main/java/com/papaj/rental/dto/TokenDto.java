package com.papaj.rental.dto;

import lombok.Getter;

@Getter
public class TokenDto {

    private final String accessToken;

    public TokenDto(final String accessToken) {
        this.accessToken = accessToken;
    }
}
