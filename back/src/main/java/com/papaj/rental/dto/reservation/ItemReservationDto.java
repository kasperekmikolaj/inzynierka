package com.papaj.rental.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class ItemReservationDto {

    private long id;
    private LocalDate date;
    private String color;
    private String bookingUserEmail;
}
