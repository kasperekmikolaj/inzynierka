package com.papaj.rental.dto.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.papaj.rental.models.Group;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GroupDto {

    private long id;
    private String name;
    private String type;
    private String image;
    private String description;

    @JsonProperty(value="isPublic")
    private Boolean isPublic;

    public GroupDto(Group group) {
        this.id = group.getId();
        this.name = group.getName();
        this.type = group.getType();
        this.image = group.getImage();
        this.description = group.getDescription();
        this.isPublic = group.getIsPublic();
    }
}
