package com.papaj.rental.dto.group;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class CreateGroupDto {

    @NotBlank
    @Size(min = 4, max = 40)
    private String name;

    @Size(max = 20)
    private String type;

    private String image;
    private String description;

    @NotNull
    private boolean isPublic;
}