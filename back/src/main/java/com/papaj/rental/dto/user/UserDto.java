package com.papaj.rental.dto.user;

import com.papaj.rental.models.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

    private Long userId;
    private String email;
    private String username;
    private String telephone;

    public UserDto(User user) {
        this.userId = user.getId();
        this.email = user.getEmail();
        this.username = user.getUsername();
        this.telephone = user.getTelephone();
    }
}
