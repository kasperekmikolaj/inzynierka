package com.papaj.rental.dto.reservation;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Getter
@Setter
public class DeleteItemReservationDto {

    @NotNull
    @NotEmpty
    private ArrayList<Long> itemReservationIdList;
}
