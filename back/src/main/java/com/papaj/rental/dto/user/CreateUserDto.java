package com.papaj.rental.dto.user;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CreateUserDto {

  @Email
  @NotBlank
  @Size(min = 5, max = 255)
  private String email;

  @NotNull
  @Size(min = 8, max = 120)
  private String password;

  @NotBlank
  @Size(max = 25, min = 4)
  private String username;

  @Size(max = 20)
  private String telephone;
}
