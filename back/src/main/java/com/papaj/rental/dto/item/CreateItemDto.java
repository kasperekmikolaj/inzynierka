package com.papaj.rental.dto.item;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CreateItemDto {

    @NotBlank
    @Size(min = 4, max = 25)
    private String name;

    private String description;
    private String image;

    @NotBlank
    private long groupId;
}
