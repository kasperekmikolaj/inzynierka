package com.papaj.rental.dto.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteItemDto {

    private long itemId;
    private long groupId;
}
