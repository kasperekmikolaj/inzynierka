package com.papaj.rental.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
public class CreateItemReservationDto {

    @NotNull
    private long itemId;

    @NotNull
    @NotEmpty
    private ArrayList<LocalDate> dates;
}
