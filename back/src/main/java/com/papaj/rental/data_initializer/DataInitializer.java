package com.papaj.rental.data_initializer;

import com.papaj.rental.models.*;
import com.papaj.rental.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;

//uncomment to initialize database
//@Component
public class DataInitializer implements CommandLineRunner {

    private final String firstMemberColor = "#14a4d9";
    private final String secondMemberColor = "#2cd911";

    private final UserRepository userRepository;
    private final GroupRepository groupRepository;
    private final ItemRepository itemRepository;
    private final ItemReservationRepository itemReservationRepository;
    private final GroupInvitationRepository groupInvitationRepository;
    private final GroupMemberRepository groupMemberRepository;

    public DataInitializer(
            UserRepository userRepository,
            GroupRepository groupRepository,
            ItemRepository itemRepository,
            ItemReservationRepository itemReservationRepository,
            GroupInvitationRepository groupInvitationRepository,
            GroupMemberRepository groupMemberRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.itemRepository = itemRepository;
        this.itemReservationRepository = itemReservationRepository;
        this.groupInvitationRepository = groupInvitationRepository;
        this.groupMemberRepository = groupMemberRepository;
    }

    @Override
    public void run(String... args) {
        System.out.println("Data initialization...");

        DataImages dataImages = new DataImages();

        String password1 = BCrypt.hashpw("papaj333", BCrypt.gensalt());
        String password2 = BCrypt.hashpw("kowalski", BCrypt.gensalt());
        String password3 = BCrypt.hashpw("janek333", BCrypt.gensalt());

        User papaj = new User("papaj333", "papaj@123.com", password1, "333-333-333", new ArrayList<>());
        User kowalski = new User("kowalski", "kowalski@123.com", password2, "222-222-222", new ArrayList<>());
        User janek = new User("janek333", "janek@123.com", password3, "111-111-111", new ArrayList<>());

        User temp1 = new User("karol21", "temp1@123.com", password3, "111-111-111", new ArrayList<>());
        User temp2 = new User("maciek99", "temp2@123.com", password3, "111-111-111", new ArrayList<>());

        this.userRepository.save(papaj);
        this.userRepository.save(kowalski);
        this.userRepository.save(janek);
        this.userRepository.save(temp1);
        this.userRepository.save(temp2);

        /*
        private group initialization
        */
        Group privateGroup = new Group("Company billboards", "Billboards", dataImages.billboardGroupImage,
                "Company billboards reservation management group", false, new ArrayList<>(), new ArrayList<>());
        GroupMember adminPrivateGroup = new GroupMember(papaj, privateGroup, UserRoleEnum.ADMIN);
        privateGroup.getMembers().add(adminPrivateGroup);

        Item billboard1 = new Item("Near Lidl supermarket",
                "The billboard is placed near the biggest supermarket in town.", privateGroup, new ArrayList<>(), dataImages.billboard1);
        Item billboard2 = new Item("On Paderewskiego street",
                "The billboard is placed on the most popular street in town.", privateGroup, new ArrayList<>(), dataImages.billboard2);

        this.groupRepository.save(privateGroup);
        this.itemRepository.save(billboard1);
        this.itemRepository.save(billboard2);

        ItemReservation res1 = new ItemReservation(LocalDate.of(2020, 9, 1), billboard1, papaj);
        ItemReservation res2 = new ItemReservation(LocalDate.of(2020, 9, 2), billboard1, papaj);
        ItemReservation res3 = new ItemReservation(LocalDate.of(2020, 9, 3), billboard1, papaj);

        ItemReservation res7 = new ItemReservation(LocalDate.of(2020, 9, 9), billboard2, papaj);
        ItemReservation res71 = new ItemReservation(LocalDate.of(2020, 9, 9), billboard1, papaj);

        ItemReservation res8 = new ItemReservation(LocalDate.of(2020, 9, 10), billboard2, papaj);
        ItemReservation res9 = new ItemReservation(LocalDate.of(2020, 9, 11), billboard2, papaj);


        this.itemReservationRepository.save(res1);
        this.itemReservationRepository.save(res2);
        this.itemReservationRepository.save(res3);
        this.itemReservationRepository.save(res7);
        this.itemReservationRepository.save(res8);
        this.itemReservationRepository.save(res9);
        this.itemReservationRepository.save(res71);

        GroupInvitation invitation1 = new GroupInvitation(kowalski, privateGroup);
        GroupInvitation invitation2 = new GroupInvitation(janek, privateGroup);

        this.groupInvitationRepository.save(invitation1);
        this.groupInvitationRepository.save(invitation2);

        GroupMember firstPrivateMember = new GroupMember(temp1, privateGroup, firstMemberColor, UserRoleEnum.MODERATOR);
        this.groupMemberRepository.save(firstPrivateMember);
        GroupMember secondPrivateMember = new GroupMember(temp2, privateGroup, secondMemberColor, UserRoleEnum.MEMBER);
        this.groupMemberRepository.save(secondPrivateMember);

        /*
        yachts public group initialization
        */
        Group yachtsPublicGroup = new Group("Yachts Mazury", "Yachts", dataImages.yachtsGroupImage,
                "Well known yachts renting company on Mazury lakes!", true, new ArrayList<>(), new ArrayList<>());
        GroupMember adminYachtsGroup = new GroupMember(kowalski, yachtsPublicGroup, UserRoleEnum.ADMIN);
        yachtsPublicGroup.getMembers().add(adminYachtsGroup);

        Item yacht1 = new Item("Antila 24",
                "Antila 24 yacht from year 2019. Small and easy to sail. Best for new not experienced sailors.",
                yachtsPublicGroup, new ArrayList<>(), dataImages.antila24);
        Item yacht2 = new Item("Omega",
                "Omega from year 2016. Very popular boat in Poland. Best for one day sailing.", yachtsPublicGroup, new ArrayList<>(), dataImages.omega);
        Item yacht3 = new Item("Antila 33",
                "Antila 33 is one of the biggest yacht form Antila family yachts. It is big and comfortable yacht for 6-8 people.", yachtsPublicGroup, new ArrayList<>(), dataImages.antila33);
        Item yacht4 = new Item("Shine 30",
                "Shine 30 is modern yacht with 3 separate cabins prepared for 6-8 people. Great choice for one-week holidays on Mazury lakes.", yachtsPublicGroup, new ArrayList<>(), dataImages.shine30);
        Item yacht5 = new Item("Maxus 28",
                "Maxus 28 is considered the fastest tourist yacht. It is bog, comfortable and very popular on Mazury lakes.", yachtsPublicGroup, new ArrayList<>(), dataImages.maxus28);

        this.groupRepository.save(yachtsPublicGroup);
        this.itemRepository.save(yacht1);
        this.itemRepository.save(yacht2);
        this.itemRepository.save(yacht3);
        this.itemRepository.save(yacht4);
        this.itemRepository.save(yacht5);

        GroupMember yachtsMember = new GroupMember(janek, yachtsPublicGroup, firstMemberColor, UserRoleEnum.MODERATOR);
        this.groupMemberRepository.save(yachtsMember);

        /*
        books public  group initialization
        */
        Group booksPublicGroup = new Group("Super książki", "Książki", dataImages.booksGroupImage,
                "Najciekawsze książki!!! Każdy znajdzie coś dla siebie.", true, new ArrayList<>(), new ArrayList<>());
        GroupMember adminBooksGroup = new GroupMember(papaj, booksPublicGroup, UserRoleEnum.ADMIN);
        booksPublicGroup.getMembers().add(adminBooksGroup);

        Item book1 = new Item("Diuna",
                "Powieść science fiction napisane przez Franka Herberta w roku 1965. Powieść sprzedano na całym świecie w ponad 20 milonach egzemplarzy.",
                booksPublicGroup, new ArrayList<>(), dataImages.diuna);
        Item book2 = new Item("Uczennica Maga",
                "Prequel popularnej trylogii Czarnego Maga napisanej przez australijską pisarkę Trudi Canavan.",
                booksPublicGroup, new ArrayList<>(), dataImages.blackMage);

        this.groupRepository.save(booksPublicGroup);
        this.itemRepository.save(book1);
        this.itemRepository.save(book2);

        /*
        cars public group initialization
        */
        Group carsPublicGroup = new Group("Papaja cars", "Cars", dataImages.carsGroupImage,
                "Jesteśmy słynną na całą Polskę firmą prowadzącą wynajem najwyższej klasy samochodów osobowych.", true, new ArrayList<>(), new ArrayList<>());
        GroupMember adminCarsGroup = new GroupMember(kowalski, carsPublicGroup, UserRoleEnum.ADMIN);
        carsPublicGroup.getMembers().add(adminCarsGroup);

        Item car1 = new Item("Jaguar xf",
                "Samochód osobowy produkowany pod brytyjską marką Jaguar od 2007 roku. Od 2015 roku produkowana jest druga generacja modelu.",
                carsPublicGroup, new ArrayList<>(), dataImages.jaguar);
        Item car2 = new Item("Lamborghini Aventador",
                "Samochód produkowany pod włoską marką Lamborghini od 2011 roku.",
                carsPublicGroup, new ArrayList<>(), dataImages.lambo);
        Item car3 = new Item("Aston Martin DB11",
                "Samochód sportowy produkowany pod brytyjską marką Aston Martin od 2016 roku.",
                carsPublicGroup, new ArrayList<>(), dataImages.aston);

        this.groupRepository.save(carsPublicGroup);
        this.itemRepository.save(car1);
        this.itemRepository.save(car2);
        this.itemRepository.save(car3);

        GroupMember carsMember = new GroupMember(janek, carsPublicGroup, firstMemberColor, UserRoleEnum.MODERATOR);
        this.groupMemberRepository.save(carsMember);

        GroupInvitation invitation11 = new GroupInvitation(papaj, carsPublicGroup);
        this.groupInvitationRepository.save(invitation11);

        /*
        fishing rod group initialization
        */
        Group fishingRodPublicGroup = new Group("The Fishing Masters", "Fishing Rod", "No image",
                "Best fishing rods for best anglers.", true, new ArrayList<>(), new ArrayList<>());
        GroupMember adminFishingRodsGroup = new GroupMember(kowalski, fishingRodPublicGroup, UserRoleEnum.ADMIN);
        fishingRodPublicGroup.getMembers().add(adminFishingRodsGroup);

        Item fishingRod1 = new Item("Red with reel",
                "Red fishing rod with reel",
                carsPublicGroup, new ArrayList<>(), "no image");
        Item fishingRod2 = new Item("Blue without reel",
                "Blue fishing rod without reel",
                carsPublicGroup, new ArrayList<>(), "no image");

        this.groupRepository.save(fishingRodPublicGroup);
        this.itemRepository.save(fishingRod1);
        this.itemRepository.save(fishingRod2);
    }
}

























