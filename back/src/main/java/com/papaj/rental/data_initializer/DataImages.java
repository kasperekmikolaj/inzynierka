package com.papaj.rental.data_initializer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class DataImages {

    public String booksGroupImage = "error";
    public String yachtsGroupImage = "error";
    public String billboardGroupImage = "error";
    public String carsGroupImage = "error";

    // items
    public String billboard1 = "error";
    public String billboard2 = "error";

    public String antila24 = "error";
    public String antila33 = "error";
    public String omega = "error";
    public String shine30 = "error";
    public String maxus28 = "error";

    public String diuna = "error";
    public String blackMage = "error";

    public String jaguar = "error";
    public String lambo = "error";
    public String aston = "error";

    public DataImages() {
        File bookFile = new File("src/main/java/com/papaj/rental/data_initializer/images/books/books_group_image.txt");
        try(BufferedReader bookStream = new BufferedReader(new FileReader(bookFile))){
            this.booksGroupImage = bookStream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File yachtFile = new File("src/main/java/com/papaj/rental/data_initializer/images/yachts/yachts_group_image.txt");
        try(BufferedReader yachtStream = new BufferedReader(new FileReader(yachtFile))){
            this.yachtsGroupImage = yachtStream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File billboardFile = new File("src/main/java/com/papaj/rental/data_initializer/images/billboards/billboard_group_image.txt");
        try(BufferedReader billboardStream = new BufferedReader(new FileReader(billboardFile))){
            this.billboardGroupImage = billboardStream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File carsFile = new File("src/main/java/com/papaj/rental/data_initializer/images/cars/cars_group_image.txt");
        try(BufferedReader carsStream = new BufferedReader(new FileReader(carsFile))){
            this.carsGroupImage = carsStream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //items
        // billboards
        File billboard1file = new File("src/main/java/com/papaj/rental/data_initializer/images/billboards/billboard_1.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(billboard1file))){
            this.billboard1 = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File billboard2file = new File("src/main/java/com/papaj/rental/data_initializer/images/billboards/billboard_2.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(billboard2file))){
            this.billboard2 = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //yachts
        File antilaFIle = new File("src/main/java/com/papaj/rental/data_initializer/images/yachts/antila.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(antilaFIle))){
            this.antila24 = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File omegaFile = new File("src/main/java/com/papaj/rental/data_initializer/images/yachts/omega.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(omegaFile))){
            this.omega = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File antila33File = new File("src/main/java/com/papaj/rental/data_initializer/images/yachts/antila33.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(antila33File))){
            this.antila33 = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File shine30File = new File("src/main/java/com/papaj/rental/data_initializer/images/yachts/shine_30.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(shine30File))){
            this.shine30 = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File maxus28File = new File("src/main/java/com/papaj/rental/data_initializer/images/yachts/maxus_28.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(maxus28File))){
            this.maxus28 = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //books
        File diunaFile = new File("src/main/java/com/papaj/rental/data_initializer/images/books/diuna.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(diunaFile))){
            this.diuna = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File blackMageFile = new File("src/main/java/com/papaj/rental/data_initializer/images/books/uczennica_maga.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(blackMageFile))){
            this.blackMage = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //cars
        File jaguarFile = new File("src/main/java/com/papaj/rental/data_initializer/images/cars/jaguar.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(jaguarFile))){
            this.jaguar = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File lamboFile = new File("src/main/java/com/papaj/rental/data_initializer/images/cars/lambo.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(lamboFile))){
            this.lambo = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File astonFile = new File("src/main/java/com/papaj/rental/data_initializer/images/cars/aston.txt");
        try(BufferedReader stream = new BufferedReader(new FileReader(astonFile))){
            this.aston = stream.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
