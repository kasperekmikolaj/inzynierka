package com.papaj.rental.controllers;

import com.papaj.rental.configuration.security.Role;
import com.papaj.rental.dto.invitation.GroupInvitationDeleteDto;
import com.papaj.rental.dto.invitation.GroupInvitationDto;
import com.papaj.rental.services.GroupInvitationService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("invitation")
public class GroupInvitationController {

    private final GroupInvitationService groupInvitationService;

    public GroupInvitationController(GroupInvitationService groupInvitationService) {
        this.groupInvitationService = groupInvitationService;
    }

    @Secured(Role.USER)
    @PostMapping
    public void addGroupInvitation(@RequestBody GroupInvitationDto groupInvitationDto, Principal principal) {
        this.groupInvitationService.addGroupInvitation(principal.getName(), groupInvitationDto);
    }

    @Secured(Role.USER)
    @PostMapping("delete")
    public void deleteGroupInvitation(@RequestBody GroupInvitationDeleteDto groupInvitationDeleteDto, Principal principal){
        this.groupInvitationService.deleteGroupInvitation(principal.getName(), groupInvitationDeleteDto);
    }

    @Secured(Role.USER)
    @PostMapping("accept")
    public void acceptInvitation(@RequestParam long groupId, Principal principal) {
        this.groupInvitationService.acceptInvitation(principal.getName(), groupId);
    }

    @Secured(Role.USER)
    @PostMapping("decline")
    public void declineInvitation(@RequestParam long groupId, Principal principal) {
        this.groupInvitationService.declineInvitation(principal.getName(), groupId);
    }
}
