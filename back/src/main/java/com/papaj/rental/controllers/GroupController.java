package com.papaj.rental.controllers;

import com.papaj.rental.configuration.security.Role;
import com.papaj.rental.dto.group.CreateGroupDto;
import com.papaj.rental.dto.group.GroupDetailsDto;
import com.papaj.rental.dto.group.GroupDto;
import com.papaj.rental.services.GroupService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("group")
public class GroupController {

    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("public")
    List<GroupDto> getAllPublicGroups() {
        return this.groupService.getAllPublicGroups();
    }

    @GetMapping("all_user")
    @Secured(Role.USER)
    List<GroupDto> getAllUserGroups(Principal principal) {
        return this.groupService.getUserGroups(principal.getName());
    }

    @GetMapping("details")
    public GroupDetailsDto getGroupDetailsForUser(@RequestParam("id") long groupId, Principal principal) {
        String mail = principal == null ? null : principal.getName();
        return this.groupService.getGroupWithDetails(groupId, mail);
    }

    @Secured(Role.USER)
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createGroup(@RequestBody CreateGroupDto createGroupDto, Principal principal) {
        this.groupService.createGroup(principal.getName(), createGroupDto);
    }

    @Secured(Role.USER)
    @PatchMapping
    public void updateGroup(@RequestBody GroupDto groupDto, Principal principal) {
        this.groupService.updateGroup(principal.getName(), groupDto);
    }

    @Secured(Role.USER)
    @DeleteMapping
    public void deleteGroup(@RequestParam("id") long groupId, Principal principal) {
        this.groupService.deleteGroup(principal.getName(), groupId);
    }

    @Secured(Role.USER)
    @GetMapping("invitations")
    public List<GroupDto> getAllGroupInvitationsForUser(Principal principal) {
        return this.groupService.getGroupsInvitingUser(principal.getName());
    }

    @Secured(Role.USER)
    @GetMapping("basic")
    public GroupDto getBasicGroupData(@RequestParam("id") long groupId) {
        return this.groupService.getBasicGroupData(groupId);
    }
}
