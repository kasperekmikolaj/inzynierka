package com.papaj.rental.controllers;

import com.papaj.rental.configuration.security.Role;
import com.papaj.rental.models.UserRoleEnum;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("user_role")
public class UserRoleController {

    @Secured(Role.USER)
    @GetMapping
    public List<String> getUserRoles() {
        return Arrays.stream(UserRoleEnum.values()).map(Enum::toString).collect(Collectors.toList());
    }
}
