package com.papaj.rental.controllers.security;

import com.papaj.rental.services.UserAuthenticationService;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("logout")
public class LogoutController {

    private static final String TOKEN_PREFIX = "Bearer ";

    private final UserAuthenticationService userAuthenticationService;

    public LogoutController(UserAuthenticationService userAuthenticationService) {
        this.userAuthenticationService = userAuthenticationService;
    }

    @PostMapping
    public void logoutUser(@RequestHeader(HttpHeaders.AUTHORIZATION) String fullToken) {
        String token = fullToken.replace(TOKEN_PREFIX, "");
        userAuthenticationService.logout(token);
    }

}