package com.papaj.rental.controllers.security;

import com.papaj.rental.dto.user.CreateUserDto;
import com.papaj.rental.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("signUp")
public class SignUpController {

    private final UserService userService;

    public SignUpController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void signUpUser(@RequestBody CreateUserDto createUserDto) {
        userService.createUser(createUserDto);
    }
}
