package com.papaj.rental.controllers;

import com.papaj.rental.configuration.security.Role;
import com.papaj.rental.dto.user.CreateUserDto;
import com.papaj.rental.dto.user.UserDto;
import com.papaj.rental.services.UserAuthenticationService;
import com.papaj.rental.services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    private static final String TOKEN_PREFIX = "Bearer ";

    private final UserService userService;
    private final UserAuthenticationService userAuthenticationService;

    public UserController(UserService userService,
                          UserAuthenticationService userAuthenticationService) {
        this.userService = userService;
        this.userAuthenticationService = userAuthenticationService;
    }

    @Secured(Role.USER)
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(Principal principal) {
        this.userService.deleteByEmail(principal.getName());
    }

    @Secured(Role.USER)
    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    public void updateUser(@RequestHeader(HttpHeaders.AUTHORIZATION) String fullToken,
                           @RequestBody CreateUserDto createUserDto, Principal principal) {
        this.userService.updateUser(principal.getName(), createUserDto);
        String token = fullToken.replace(TOKEN_PREFIX, "");
        this.userAuthenticationService.logout(token);
    }

    @Secured(Role.USER)
    @GetMapping
    public UserDto getUserData(Principal principal) {
        String email = principal.getName();
        return new UserDto(this.userService.getUser(email));
    }

    @Secured(Role.USER)
    @GetMapping("invitation")
    public List<UserDto> getUsersInvitedToGroup(@RequestParam long groupId, Principal principal) {
        return this.userService.getUsersInvitedToGroup(principal.getName(), groupId);
    }
}
