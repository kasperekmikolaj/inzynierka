package com.papaj.rental.controllers;

import com.papaj.rental.configuration.security.Role;
import com.papaj.rental.dto.item.ItemWithReservationsDto;
import com.papaj.rental.dto.reservation.CreateItemReservationDto;
import com.papaj.rental.dto.reservation.DeleteItemReservationDto;
import com.papaj.rental.services.ItemService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("item_reservation")
public class ItemReservationController {

    private final ItemService itemService;

    public ItemReservationController(ItemService itemService) {
        this.itemService = itemService;
    }

    @Secured(Role.USER)
    @PostMapping
    public ItemWithReservationsDto addReservation(@RequestBody CreateItemReservationDto createItemReservationDto, Principal principal) {
        return this.itemService.addReservation(principal.getName(), createItemReservationDto);
    }

    @Secured(Role.USER)
    @PostMapping("delete")
    public ItemWithReservationsDto deleteReservation(@RequestBody DeleteItemReservationDto deleteItemReservationDto, Principal principal) {
        return this.itemService.deleteReservation(principal.getName(), deleteItemReservationDto);
    }
}
