package com.papaj.rental.controllers.security;

import com.papaj.rental.dto.LoginDto;
import com.papaj.rental.dto.TokenDto;
import com.papaj.rental.services.UserAuthenticationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("login")
public class LoginController {

    private final UserAuthenticationService userAuthenticationService;

    public LoginController(UserAuthenticationService userAuthenticationService) {
        this.userAuthenticationService = userAuthenticationService;
    }

    @PostMapping
    public TokenDto loginUser(@RequestBody LoginDto loginDto) {
        return userAuthenticationService.login(loginDto);
    }

}
