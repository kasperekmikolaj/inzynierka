package com.papaj.rental.controllers;

import com.papaj.rental.configuration.security.Role;
import com.papaj.rental.dto.group_member.GroupMemberDeleteDto;
import com.papaj.rental.dto.group_member.GroupMemberDto;
import com.papaj.rental.dto.group_member.GroupMemberPatchColorDto;
import com.papaj.rental.dto.group_member.GroupMemberPatchRoleDto;
import com.papaj.rental.services.GroupMemberService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("group_member")
public class GroupMemberController {

    private final GroupMemberService groupMemberService;

    public GroupMemberController(GroupMemberService groupMemberService) {
        this.groupMemberService = groupMemberService;
    }

    @Secured(Role.USER)
    @PatchMapping("color")
    public void changeUserColor(@RequestBody GroupMemberPatchColorDto colorDto, Principal principal) {
        this.groupMemberService.changeMemberColor(principal.getName(), colorDto);
    }

    @Secured(Role.USER)
    @PatchMapping("role")
    public void changeUserRole(@RequestBody GroupMemberPatchRoleDto roleDto, Principal principal) {
        this.groupMemberService.changeMemberRole(principal.getName(), roleDto);
    }

    @Secured(Role.USER)
    @PostMapping("delete")
    public void deleteUserFromGroup(@RequestBody GroupMemberDeleteDto deleteDto, Principal principal) {
        this.groupMemberService.deleteUserFromGroup(principal.getName(), deleteDto);
    }

    @Secured(Role.USER)
    @GetMapping("public_group")
    public List<GroupMemberDto> getMembersOfPublicGroup(@RequestParam long groupId, Principal principal) {
        return this.groupMemberService.getMembersOfPublicGroup(groupId, principal.getName());
    }

    @Secured(Role.USER)
    @DeleteMapping("leave_group")
    public void leaveGroup(@RequestParam long groupId, Principal principal) {
        this.groupMemberService.leaveGroup(groupId, principal.getName());
    }

    @Secured(Role.USER)
    @GetMapping("all")
    public List<GroupMemberDto> getAllGroupMembers(@RequestParam long groupId, Principal principal) {
        return this.groupMemberService.getAllGroupMembers(groupId, principal.getName());
    }
}
