package com.papaj.rental.controllers;

import com.papaj.rental.configuration.security.Role;
import com.papaj.rental.dto.item.CreateItemDto;
import com.papaj.rental.dto.item.DeleteItemDto;
import com.papaj.rental.dto.item.ItemDto;
import com.papaj.rental.dto.item.ItemWithReservationsDto;
import com.papaj.rental.models.Item;
import com.papaj.rental.services.GroupMemberService;
import com.papaj.rental.services.ItemService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("item")
public class ItemController {

    private final ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("with_reservations")
    public ItemWithReservationsDto getItemWithReservationData(@RequestParam("itemId") long itemId, Principal principal) {
        return this.itemService.getItemWithReservations(itemId);
    }

    @Secured(Role.USER)
    @PostMapping
    public long addItemToGroup(@RequestBody CreateItemDto createItemDto, Principal principal) {
        return this.itemService.addItemToGroup(principal.getName(), createItemDto);
    }

    @Secured(Role.USER)
    @PostMapping("delete")
    public void deleteItem(@RequestBody DeleteItemDto deleteItemDto, Principal principal) {
        this.itemService.deleteItem(principal.getName(), deleteItemDto);
    }

    @Secured(Role.USER)
    @PatchMapping
    public void updateItem(@RequestBody ItemDto itemDto, Principal principal) {
        this.itemService.updateItem(principal.getName(), itemDto);
    }

    @GetMapping("group")
    public List<ItemDto> getItemsForGroup(@RequestParam long groupId, Principal principal) {
        String mail = principal == null ? null : principal.getName();
        return this.itemService.getItemsForGroup(mail, groupId);
    }
}
