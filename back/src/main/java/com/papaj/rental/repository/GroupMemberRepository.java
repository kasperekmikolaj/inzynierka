package com.papaj.rental.repository;

import com.papaj.rental.models.GroupMember;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupMemberRepository extends CrudRepository<GroupMember, Long> {

    List<GroupMember> findAllByUser_Email(String email);

    Optional<GroupMember> findByUser_EmailAndGroup_Id(String userEmail, long groupId);

    Optional<GroupMember> findByUser_IdAndGroup_Id(long userId, long groupId);

    void deleteByUser_IdAndGroup_Id(long userId, long groupId);

    void deleteByUser_EmailAndGroup_Id(String userEmail, long groupId);

    List<GroupMember> findAllByGroup_Id(long groupId);
}
