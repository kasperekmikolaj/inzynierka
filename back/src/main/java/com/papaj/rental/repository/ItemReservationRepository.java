package com.papaj.rental.repository;

import com.papaj.rental.models.ItemReservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemReservationRepository extends CrudRepository<ItemReservation, Long> {

    void deleteAllByBookingUserId(long bookingUserId);
}
