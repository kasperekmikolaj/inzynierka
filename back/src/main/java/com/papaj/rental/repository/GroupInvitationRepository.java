package com.papaj.rental.repository;

import com.papaj.rental.models.GroupInvitation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupInvitationRepository extends CrudRepository<GroupInvitation, Long> {

    void deleteByUser_EmailAndGroup_Id(String userEmail, long groupId);

    void deleteByUser_IdAndGroup_Id(long userId, long groupId);

    List<GroupInvitation> findAllByUser_Email(String userEmail);

    List<GroupInvitation> findAllByGroup_Id(long groupId);
}
