package com.papaj.rental.services.impl;

import com.papaj.rental.dto.group.CreateGroupDto;
import com.papaj.rental.dto.group.GroupDetailsDto;
import com.papaj.rental.dto.group.GroupDto;
import com.papaj.rental.exceptions.NotFoundException;
import com.papaj.rental.exceptions.UnauthorizedException;
import com.papaj.rental.models.Group;
import com.papaj.rental.models.GroupMember;
import com.papaj.rental.models.User;
import com.papaj.rental.models.UserRoleEnum;
import com.papaj.rental.repository.GroupInvitationRepository;
import com.papaj.rental.repository.GroupMemberRepository;
import com.papaj.rental.repository.GroupRepository;
import com.papaj.rental.services.GroupMemberService;
import com.papaj.rental.services.GroupService;
import com.papaj.rental.services.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class GroupServiceImpl implements GroupService {

    private final GroupMemberRepository groupMemberRepository;
    private final GroupRepository groupRepository;
    private final UserService userService;
    private final GroupMemberService groupMemberService;
    private final GroupInvitationRepository groupInvitationRepository;

    public GroupServiceImpl(GroupMemberRepository groupMemberRepository,
                            GroupRepository groupRepository,
                            UserService userService,
                            GroupMemberService groupMemberService,
                            GroupInvitationRepository groupInvitationRepository) {
        this.groupMemberRepository = groupMemberRepository;
        this.groupRepository = groupRepository;
        this.userService = userService;
        this.groupMemberService = groupMemberService;
        this.groupInvitationRepository = groupInvitationRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<GroupDto> getUserGroups(String userEmail) {
        return this.groupMemberRepository
                .findAllByUser_Email(userEmail)
                .stream()
                .map(GroupMember::getGroup)
                .map(GroupDto::new)
                .collect(Collectors.toList());
    }

    @Override
    public GroupDetailsDto getGroupWithDetails(long groupId, String userEmail) {
        Group group = this.groupRepository.findById(groupId).orElseThrow(NotFoundException::new);
        GroupMember groupMember = this.groupMemberRepository
                .findByUser_EmailAndGroup_Id(userEmail, groupId)
                .orElse(null);
        if (groupMember != null) {
            return new GroupDetailsDto(group, groupMember.getUserRole().toString(), groupMember.getUserColor(), true);
        } else if (group.getIsPublic()) {
            return new GroupDetailsDto(group, null, null, false);
        } else {
            throw new UnauthorizedException();
        }
    }

    @Override
    public void createGroup(String userEmail, CreateGroupDto createGroupDto) {
        Group newGroup = new Group(createGroupDto);
        User groupAdmin = this.userService.getUser(userEmail);
        GroupMember newGroupMember = new GroupMember(groupAdmin, newGroup, UserRoleEnum.ADMIN);
        newGroup.getMembers().add(newGroupMember);
        this.groupRepository.save(newGroup);
    }

    @Override
    public void updateGroup(String userEmail, GroupDto groupDto) {
        if (!this.groupMemberService.isAllowedToModifyGroup(userEmail, groupDto.getId())) {
            throw new UnauthorizedException();
        }
        this.groupRepository.save(this.modifyGroup(groupDto));
    }

    @Override
    public void deleteGroup(String userEmail, long groupId) {
        if (!this.groupMemberService.isAllowedToDeleteGroup(userEmail, groupId)) {
            throw new UnauthorizedException();
        }
        this.groupRepository.deleteById(groupId);
    }

    @Override
    public List<GroupDto> getAllPublicGroups() {
        return StreamSupport
                .stream(this.groupRepository.findAll().spliterator(), false)
                .filter(Group::getIsPublic)
                .map(GroupDto::new)
                .collect(Collectors.toList());
    }

    private Group modifyGroup(GroupDto groupDto) {
        Group toModify = this.groupRepository.findById(groupDto.getId()).orElseThrow(NotFoundException::new);

        String newName = groupDto.getName();
        if (newName != null) {
            toModify.setName(newName);
        }
        String newType = groupDto.getType();
        if (newType != null) {
            toModify.setType(newType);
        }
        String newImage = groupDto.getImage();
        if (newImage != null) {
            toModify.setImage(newImage);
        }
        String newDescription = groupDto.getDescription();
        if (newDescription != null) {
            toModify.setDescription(newDescription);
        }
        Boolean isPublic = groupDto.getIsPublic();
        if (isPublic != null) {
            toModify.setIsPublic(isPublic);
        }
        return toModify;
    }

    @Override
    public List<GroupDto> getGroupsInvitingUser(String userEmail) {
        return this.groupInvitationRepository
                .findAllByUser_Email(userEmail)
                .stream()
                .map(groupInvitation -> new GroupDto(groupInvitation.getGroup()))
                .collect(Collectors.toList());
    }

    @Override
    public GroupDto getBasicGroupData(long groupId) {
        return new GroupDto(this.groupRepository.findById(groupId).orElseThrow(NotFoundException::new));
    }
}
