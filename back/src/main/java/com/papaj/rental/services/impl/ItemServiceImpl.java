package com.papaj.rental.services.impl;

import com.papaj.rental.dto.item.CreateItemDto;
import com.papaj.rental.dto.item.DeleteItemDto;
import com.papaj.rental.dto.item.ItemDto;
import com.papaj.rental.dto.item.ItemWithReservationsDto;
import com.papaj.rental.dto.reservation.CreateItemReservationDto;
import com.papaj.rental.dto.reservation.DeleteItemReservationDto;
import com.papaj.rental.dto.reservation.ItemReservationDto;
import com.papaj.rental.exceptions.NotFoundException;
import com.papaj.rental.exceptions.UnauthorizedException;
import com.papaj.rental.models.Group;
import com.papaj.rental.models.Item;
import com.papaj.rental.models.ItemReservation;
import com.papaj.rental.models.User;
import com.papaj.rental.repository.GroupRepository;
import com.papaj.rental.repository.ItemRepository;
import com.papaj.rental.repository.ItemReservationRepository;
import com.papaj.rental.repository.UserRepository;
import com.papaj.rental.services.GroupMemberService;
import com.papaj.rental.services.ItemService;
import com.papaj.rental.services.UtilsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;
    private final GroupRepository groupRepository;
    private final UtilsService utilsService;
    private final UserRepository userRepository;
    private final ItemReservationRepository itemReservationRepository;
    private final GroupMemberService groupMemberService;

    public ItemServiceImpl(ItemRepository itemRepository,
                           GroupRepository groupRepository,
                           UtilsService utilsService,
                           UserRepository userRepository,
                           ItemReservationRepository itemReservationRepository,
                           GroupMemberService groupMemberService) {
        this.itemRepository = itemRepository;
        this.groupRepository = groupRepository;
        this.utilsService = utilsService;
        this.userRepository = userRepository;
        this.itemReservationRepository = itemReservationRepository;
        this.groupMemberService = groupMemberService;
    }


    @Override
    public Item getItemById(long id) {
        return this.itemRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public long addItemToGroup(String loggedUserEmail, CreateItemDto createItemDto) {
        if (!this.utilsService.isUserAGroupAdminOrModerator(loggedUserEmail, createItemDto.getGroupId())) {
            throw new UnauthorizedException();
        }
        Group group = this.groupRepository.findById(createItemDto.getGroupId()).orElseThrow(NotFoundException::new);
        Item newItem = new Item(createItemDto.getName(), createItemDto.getDescription(), group, new ArrayList<>(), createItemDto.getImage());
        return this.itemRepository.save(newItem).getId();
    }

    @Override
    public void deleteItem(String loggedUserEmail, DeleteItemDto deleteItemDto) {
        if (!this.utilsService.isUserAGroupAdminOrModerator(loggedUserEmail, deleteItemDto.getGroupId())) {
            throw new UnauthorizedException();
        }
        this.itemRepository.deleteById(deleteItemDto.getItemId());
    }

    @Override
    public ItemWithReservationsDto addReservation(String loggedUserEmail, CreateItemReservationDto createItemReservationDto) {
        Item reservedItem = itemRepository.findById(createItemReservationDto.getItemId()).orElseThrow(NotFoundException::new);
        long itemGroupId = reservedItem.getGroup().getId();

        if (!this.utilsService.isUserAllowedToReserveItemsInGroup(loggedUserEmail, itemGroupId)) {
            throw new UnauthorizedException();
        }

        User bookingUser = userRepository.findByEmail(loggedUserEmail).orElseThrow(NotFoundException::new);
        for(LocalDate resDate: createItemReservationDto.getDates()) {
            ItemReservation newReservation = new ItemReservation(resDate, reservedItem, bookingUser);
            this.itemReservationRepository.save(newReservation);
        }

        List<ItemReservationDto> reservations = this.getItemReservationsAsDto(reservedItem.getReservations(), reservedItem.getGroup().getId());

        return new ItemWithReservationsDto(reservedItem, reservations);
    }

    @Override
    public ItemWithReservationsDto deleteReservation(String loggedUserEmail, DeleteItemReservationDto deleteItemReservationDto) {
        ItemReservation res =
                this.itemReservationRepository.findById(deleteItemReservationDto.getItemReservationIdList().get(0))
                .orElseThrow(NotFoundException::new);
        long itemId = res.getItem().getId();

        this.utilsService.deleteReservationsFromList(deleteItemReservationDto.getItemReservationIdList(), loggedUserEmail);

        Item item = this.itemRepository.findById(itemId).orElseThrow(NotFoundException::new);
        return new ItemWithReservationsDto(item, this.getItemReservationsAsDto(item.getReservations(), item.getGroup().getId()));
    }

    @Override
    public ItemWithReservationsDto getItemWithReservations(long itemId) {
        Item item = this.itemRepository.findById(itemId).orElseThrow(NotFoundException::new);
        ArrayList<ItemReservationDto> reservationsDto = this.getItemReservationsAsDto(item.getReservations(), item.getGroup().getId());
        return new ItemWithReservationsDto(item, reservationsDto);
    }

    @Override
    public void updateItem(String loggedUserEmail, ItemDto itemDto) {
        Item toUpdate = this.itemRepository.findById(itemDto.getId()).orElseThrow(NotFoundException::new);
        Group itemGroup = toUpdate.getGroup();

        if (!this.utilsService.isUserAGroupAdminOrModerator(loggedUserEmail, itemGroup.getId())) {
            throw new UnauthorizedException();
        }

        String newName = itemDto.getName();
        String newDescription = itemDto.getDescription();
        String newImage = itemDto.getImage();

        if (newName != null) {
            toUpdate.setName(newName);
        }
        if (newDescription != null) {
            toUpdate.setDescription(newDescription);
        }
        if (newImage != null) {
            toUpdate.setImage(newImage);
        }
        this.itemRepository.save(toUpdate);
    }

    @Override
    public List<ItemDto> getItemsForGroup(String loggedUserEmail, long groupId) {
        Group group = this.groupRepository.findById(groupId).orElseThrow(NotFoundException::new);
        if (!group.getIsPublic() && (loggedUserEmail == null || !this.utilsService.isUserAllowedToReserveItemsInGroup(loggedUserEmail, groupId))) {
            throw new UnauthorizedException();
        }
        return this.itemRepository.findAllByGroup_Id(groupId).stream().map(ItemDto::new).collect(Collectors.toList());
    }

    private ArrayList<ItemReservationDto> getItemReservationsAsDto(List<ItemReservation> reservations, long groupId) {
        ArrayList<ItemReservationDto> reservationDtoList = new ArrayList<>();
        for (ItemReservation reservation : reservations) {
            String bookingUserEmail = reservation.getBookingUser().getEmail();
            String reservationColor = this.groupMemberService.getUserColorForGroup(bookingUserEmail, groupId);
            reservationDtoList.add(
                    new ItemReservationDto(
                            reservation.getId(),
                            reservation.getReservationDate(),
                            reservationColor,
                            bookingUserEmail)
            );
        }
        return reservationDtoList;
    }
}
