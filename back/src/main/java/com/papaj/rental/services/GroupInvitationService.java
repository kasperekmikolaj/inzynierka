package com.papaj.rental.services;

import com.papaj.rental.dto.invitation.GroupInvitationDeleteDto;
import com.papaj.rental.dto.invitation.GroupInvitationDto;

public interface GroupInvitationService {

    void addGroupInvitation(String loggedUserEmail, GroupInvitationDto groupInvitationDto);

    void deleteGroupInvitation(String loggedUserEmail, GroupInvitationDeleteDto groupInvitationDeleteDto);

    void acceptInvitation(String loggedUserEmail, long groupId);

    void declineInvitation(String loggedUserEmail, long groupId);
}
