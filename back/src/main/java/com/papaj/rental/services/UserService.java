package com.papaj.rental.services;

import com.papaj.rental.dto.user.CreateUserDto;
import com.papaj.rental.dto.user.UserDto;
import com.papaj.rental.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> findByEmail(String email);

    void createUser(CreateUserDto createUserDto);

    void deleteByEmail(String email);

    void updateUser(String userEmail, CreateUserDto updateUserDto);

    Long getUserId(String email);

    User getUser(String email);

    List<UserDto> getUsersInvitedToGroup(String loggedUserEmail, long groupId);
}
