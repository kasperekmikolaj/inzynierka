package com.papaj.rental.services.impl;

import com.papaj.rental.dto.user.CreateUserDto;
import com.papaj.rental.dto.user.UserDto;
import com.papaj.rental.exceptions.EmailExistException;
import com.papaj.rental.exceptions.NotFoundException;
import com.papaj.rental.exceptions.UnauthorizedException;
import com.papaj.rental.exceptions.UsernameExistException;
import com.papaj.rental.models.User;
import com.papaj.rental.repository.GroupInvitationRepository;
import com.papaj.rental.repository.UserRepository;
import com.papaj.rental.services.UserService;
import com.papaj.rental.services.UtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UtilsService utilsService;
    private final GroupInvitationRepository groupInvitationRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UtilsService utilsService,
                           GroupInvitationRepository groupInvitationRepository) {
        this.userRepository = userRepository;
        this.utilsService = utilsService;
        this.groupInvitationRepository = groupInvitationRepository;
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void createUser(CreateUserDto createUserDto) {
        User userToSave = new User();

        String newEmail = createUserDto.getEmail();
        String newUsername = createUserDto.getUsername();

        if (userRepository.existsByEmail(newEmail)) {
            throw new EmailExistException();
        }

        if (userRepository.existsByUsername(newUsername)) {
            throw new UsernameExistException();
        }

        userToSave.setEmail(newEmail);
        String encodedPassword = BCrypt.hashpw(createUserDto.getPassword(), BCrypt.gensalt());
        userToSave.setPassword(encodedPassword);
        userToSave.setUsername(newUsername);
        userToSave.setTelephone(createUserDto.getTelephone());

        userRepository.save(userToSave);
    }

    @Override
    public void deleteByEmail(String email) {
        this.userRepository.deleteByEmail(email);
    }

    @Override
    public void updateUser(String currentEmail, CreateUserDto createUserDto) {
        User userToChange = this.userRepository.findByEmail(currentEmail).orElseThrow(NotFoundException::new);
        String newEmail = createUserDto.getEmail();
        String newPassword = createUserDto.getPassword();
        String newUsername = createUserDto.getUsername();
        String newTelephone = createUserDto.getTelephone();
        if (newEmail != null) {
            userToChange.setEmail(newEmail);
        }
        if (newPassword != null) {
            String encodedPassword = BCrypt.hashpw(createUserDto.getPassword(), BCrypt.gensalt());
            userToChange.setPassword(encodedPassword);
        }
        if (newUsername != null) {
            userToChange.setUsername(newUsername);
        }
        if (newTelephone != null) {
            userToChange.setTelephone(newTelephone);
        }
        this.userRepository.save(userToChange);
    }

    @Override
    public Long getUserId(String email) {
        return this.userRepository.findByEmail(email).orElseThrow(NotFoundException::new).getId();
    }

    @Override
    public User getUser(String email) {
        return this.userRepository.findByEmail(email).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<UserDto> getUsersInvitedToGroup(String loggedUserEmail, long groupId) {
        if (!this.utilsService.isUserAGroupAdmin(loggedUserEmail, groupId)) {
            throw new UnauthorizedException();
        }

        return this.groupInvitationRepository.findAllByGroup_Id(groupId)
                .stream()
                .map(groupInvitation -> new UserDto(groupInvitation.getUser()))
                .collect(Collectors.toList());
    }

}
