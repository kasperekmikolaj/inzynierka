package com.papaj.rental.services;

import java.util.ArrayList;

public interface UtilsService {

    boolean isUserAGroupAdmin(String userEmail, long groupId);

    boolean isUserAGroupAdminOrModerator(String userEmail, long groupId);

    boolean isUserAllowedToReserveItemsInGroup(String userEmail, long groupId);

    void deleteReservationsFromList(ArrayList<Long> reservationsList, String loggedUserEmail);
}
