package com.papaj.rental.services;

import com.papaj.rental.dto.group_member.GroupMemberDeleteDto;
import com.papaj.rental.dto.group_member.GroupMemberDto;
import com.papaj.rental.dto.group_member.GroupMemberPatchColorDto;
import com.papaj.rental.dto.group_member.GroupMemberPatchRoleDto;

import java.util.List;

public interface GroupMemberService {

    String getUserColorForGroup(String userEmail, long groupId);

    boolean isAllowedToModifyGroup(String userEmail, long groupId);

    boolean isAllowedToDeleteGroup(String userEmail, long groupId);

    void changeMemberColor(String loggedUserEmail, GroupMemberPatchColorDto colorDto);

    void changeMemberRole(String loggedUserEmail, GroupMemberPatchRoleDto roleDto);

    void deleteUserFromGroup(String loggedUserEmail, GroupMemberDeleteDto deleteDto);

    List<GroupMemberDto> getMembersOfPublicGroup(long groupId, String loggedUserEmail);

    void leaveGroup(long groupId, String loggedUserEmail);

    List<GroupMemberDto> getAllGroupMembers(long groupId, String loggedUserEmail);
}
