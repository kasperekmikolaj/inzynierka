package com.papaj.rental.services;

import com.papaj.rental.dto.group.CreateGroupDto;
import com.papaj.rental.dto.group.GroupDetailsDto;
import com.papaj.rental.dto.group.GroupDto;

import java.util.List;

public interface GroupService {

    List<GroupDto> getUserGroups(String userEmail);

    GroupDetailsDto getGroupWithDetails(long groupId, String userEmail);

    void createGroup(String userEmail, CreateGroupDto createGroupDto);

    void updateGroup(String userEmail, GroupDto groupDto);

    void deleteGroup(String userEmail, long groupId);

    List<GroupDto> getAllPublicGroups();

    List<GroupDto> getGroupsInvitingUser(String userEmail);

    GroupDto getBasicGroupData(long groupId);
}
