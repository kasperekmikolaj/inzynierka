package com.papaj.rental.services.impl;

import com.papaj.rental.dto.invitation.GroupInvitationDeleteDto;
import com.papaj.rental.dto.invitation.GroupInvitationDto;
import com.papaj.rental.exceptions.NotFoundException;
import com.papaj.rental.exceptions.UnauthorizedException;
import com.papaj.rental.exceptions.UserAlreadyGroupMemberException;
import com.papaj.rental.models.*;
import com.papaj.rental.repository.GroupInvitationRepository;
import com.papaj.rental.repository.GroupMemberRepository;
import com.papaj.rental.repository.GroupRepository;
import com.papaj.rental.repository.UserRepository;
import com.papaj.rental.services.GroupInvitationService;
import com.papaj.rental.services.UtilsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GroupInvitationServiceImpl implements GroupInvitationService {

    private final GroupInvitationRepository groupInvitationRepository;
    private final GroupMemberRepository groupMemberRepository;
    private final UserRepository userRepository;
    private final UtilsService utilsService;
    private final GroupRepository groupRepository;

    public GroupInvitationServiceImpl(GroupInvitationRepository groupInvitationRepository,
                                      GroupMemberRepository groupMemberRepository,
                                      UserRepository userRepository,
                                      UtilsService utilsService,
                                      GroupRepository groupRepository) {
        this.groupInvitationRepository = groupInvitationRepository;
        this.groupMemberRepository = groupMemberRepository;
        this.userRepository = userRepository;
        this.utilsService = utilsService;
        this.groupRepository = groupRepository;
    }

    @Override
    public void addGroupInvitation(String loggedUserEmail, GroupInvitationDto groupInvitationDto) {
        if (!this.utilsService.isUserAGroupAdmin(loggedUserEmail, groupInvitationDto.getGroupId())) {
            throw new UnauthorizedException();
        }

        boolean userAlreadyGroupMember =
                this.groupMemberRepository
                        .findByUser_EmailAndGroup_Id(groupInvitationDto.getInvitedUserEmail(),
                                groupInvitationDto.getGroupId())
                        .isPresent();
        if (userAlreadyGroupMember) {
            throw new UserAlreadyGroupMemberException();
        }

        Group invitingGroup = this.groupRepository.findById(groupInvitationDto.getGroupId()).orElseThrow(NotFoundException::new);
        User invitedUser = this.userRepository.findByEmail(groupInvitationDto.getInvitedUserEmail()).orElseThrow(NotFoundException::new);
        GroupInvitation invitation = new GroupInvitation(invitedUser, invitingGroup);
        this.groupInvitationRepository.save(invitation);
    }

    @Override
    public void deleteGroupInvitation(String loggedUserEmail, GroupInvitationDeleteDto groupInvitationDeleteDto) {
        if (!this.utilsService.isUserAGroupAdmin(loggedUserEmail, groupInvitationDeleteDto.getGroupId())) {
            throw new UnauthorizedException();
        }
        this.groupInvitationRepository
                .deleteByUser_IdAndGroup_Id(groupInvitationDeleteDto.getInvitedUserId(), groupInvitationDeleteDto.getGroupId());
    }

    @Override
    public void acceptInvitation(String loggedUserEmail, long groupId) {
        this.groupInvitationRepository.deleteByUser_EmailAndGroup_Id(loggedUserEmail, groupId);

        Group group = this.groupRepository.findById(groupId).orElseThrow(NotFoundException::new);
        User user = this.userRepository.findByEmail(loggedUserEmail).orElseThrow(NotFoundException::new);
        GroupMember groupMember = new GroupMember(user, group, UserRoleEnum.MEMBER);
        this.groupMemberRepository.save(groupMember);
    }

    @Override
    public void declineInvitation(String loggedUserEmail, long groupId) {
        this.groupInvitationRepository.deleteByUser_EmailAndGroup_Id(loggedUserEmail, groupId);
    }
}
