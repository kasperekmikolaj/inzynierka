package com.papaj.rental.services.impl;

import com.papaj.rental.dto.group_member.GroupMemberDeleteDto;
import com.papaj.rental.dto.group_member.GroupMemberDto;
import com.papaj.rental.dto.group_member.GroupMemberPatchColorDto;
import com.papaj.rental.dto.group_member.GroupMemberPatchRoleDto;
import com.papaj.rental.exceptions.GroupNotPublicException;
import com.papaj.rental.exceptions.NotFoundException;
import com.papaj.rental.exceptions.UnauthorizedException;
import com.papaj.rental.models.Group;
import com.papaj.rental.models.GroupMember;
import com.papaj.rental.models.User;
import com.papaj.rental.models.UserRoleEnum;
import com.papaj.rental.repository.GroupMemberRepository;
import com.papaj.rental.repository.GroupRepository;
import com.papaj.rental.repository.ItemReservationRepository;
import com.papaj.rental.repository.UserRepository;
import com.papaj.rental.services.GroupMemberService;
import com.papaj.rental.services.UtilsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class GroupMemberServiceImpl implements GroupMemberService {

    private final GroupMemberRepository groupMemberRepository;
    private final GroupRepository groupRepository;
    private final UtilsService utilsService;
    private final UserRepository userRepository;
    private final ItemReservationRepository itemReservationRepository;

    public GroupMemberServiceImpl(GroupMemberRepository groupMemberRepository,
                                  UtilsService utilsService,
                                  GroupRepository groupRepository,
                                  UserRepository userRepository,
                                  ItemReservationRepository itemReservationRepository) {
        this.groupMemberRepository = groupMemberRepository;
        this.utilsService = utilsService;
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
        this.itemReservationRepository = itemReservationRepository;
    }

    @Override
    public String getUserColorForGroup(String userEmail, long groupId) {
        return this.groupMemberRepository
                .findByUser_EmailAndGroup_Id(userEmail, groupId)
                .orElseThrow(NotFoundException::new).getUserColor();
    }

    @Override
    public boolean isAllowedToModifyGroup(String userEmail, long groupId) {
        GroupMember groupMember =
                this.groupMemberRepository
                        .findByUser_EmailAndGroup_Id(userEmail, groupId)
                        .orElseThrow(NotFoundException::new);
        return groupMember.getUserRole() == UserRoleEnum.ADMIN;
    }

    @Override
    public boolean isAllowedToDeleteGroup(String userEmail, long groupId) {
        GroupMember groupMember =
                this.groupMemberRepository
                        .findByUser_EmailAndGroup_Id(userEmail, groupId)
                        .orElseThrow(NotFoundException::new);
        return groupMember.getUserRole() == UserRoleEnum.ADMIN;
    }

    @Override
    public void changeMemberColor(String loggedUserEmail, GroupMemberPatchColorDto colorDto) {
        User userToChange = this.userRepository.findById(colorDto.getUserToChangeId()).orElseThrow(NotFoundException::new);
        if(loggedUserEmail.equals(userToChange.getEmail())
                || this.utilsService.isUserAGroupAdmin(loggedUserEmail, colorDto.getGroupId())) {

            GroupMember toChange =
                    this.groupMemberRepository
                            .findByUser_IdAndGroup_Id(colorDto.getUserToChangeId(), colorDto.getGroupId())
                            .orElseThrow(NotFoundException::new);
            toChange.setUserColor(colorDto.getNewColor());
            this.groupMemberRepository.save(toChange);
        } else {
            throw new UnauthorizedException();
        }
    }

    @Override
    public void changeMemberRole(String loggedUserEmail, GroupMemberPatchRoleDto roleDto) {
        if (!this.utilsService.isUserAGroupAdmin(loggedUserEmail, roleDto.getGroupId())) {
            throw new UnauthorizedException();
        }
        GroupMember toChange =
                this.groupMemberRepository
                        .findByUser_IdAndGroup_Id(roleDto.getUserToChangeId(), roleDto.getGroupId())
                        .orElseThrow(NotFoundException::new);
        if (toChange.getUser().getEmail().equals(loggedUserEmail)) {
            throw new UnauthorizedException();
        }
        toChange.setUserRole(roleDto.getRole());
        this.groupMemberRepository.save(toChange);
    }

    @Override
    public void deleteUserFromGroup(String loggedUserEmail, GroupMemberDeleteDto deleteDto) {
        if (!this.utilsService.isUserAGroupAdmin(loggedUserEmail, deleteDto.getGroupId())) {
            throw new UnauthorizedException();
        }
        this.itemReservationRepository.deleteAllByBookingUserId(deleteDto.getUserId());
        this.groupMemberRepository.deleteByUser_IdAndGroup_Id(deleteDto.getUserId(), deleteDto.getGroupId());
    }

    @Override
    public List<GroupMemberDto> getMembersOfPublicGroup(long groupId, String loggedUserEmail) {
        if (!this.utilsService.isUserAGroupAdmin(loggedUserEmail, groupId)) {
            throw new UnauthorizedException();
        }
        Group group = this.groupRepository.findById(groupId).orElseThrow(NotFoundException::new);
        if (!group.getIsPublic()) {
            throw new GroupNotPublicException();
        }
        return this.groupMemberRepository.findAllByGroup_Id(groupId).stream().map(GroupMemberDto::new).collect(Collectors.toList());
    }

    @Override
    public void leaveGroup(long groupId, String loggedUserEmail) {
        groupMemberRepository.deleteByUser_EmailAndGroup_Id(loggedUserEmail, groupId);
    }

    @Override
    public List<GroupMemberDto> getAllGroupMembers(long groupId, String loggedUserEmail) {
        if (!this.utilsService.isUserAGroupAdmin(loggedUserEmail, groupId)) {
            throw new UnauthorizedException();
        }
        return this.groupMemberRepository.findAllByGroup_Id(groupId).stream().map(GroupMemberDto::new).collect(Collectors.toList());
    }
}
