package com.papaj.rental.services.impl;

import com.papaj.rental.configuration.security.properties.JwtBlacklist;
import com.papaj.rental.configuration.security.properties.JwtProperties;
import com.papaj.rental.dto.LoginDto;
import com.papaj.rental.dto.TokenDto;
import com.papaj.rental.exceptions.FailedAuthorizationException;
import com.papaj.rental.models.User;
import com.papaj.rental.services.UserAuthenticationService;
import com.papaj.rental.services.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class UserAuthenticationServiceImpl implements UserAuthenticationService {

    private static final String TOKEN_TYPE = "JWT";

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtProperties jwtProperties;
    private final JwtBlacklist blacklist;

    @Autowired
    public UserAuthenticationServiceImpl(UserService userService,
                                         PasswordEncoder passwordEncoder,
                                         JwtProperties jwtProperties,
                                         JwtBlacklist blacklist) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.jwtProperties = jwtProperties;
        this.blacklist = blacklist;
    }

    @Override
    public TokenDto login(LoginDto loginDto) {
        Optional<User> user = userService.findByEmail(loginDto.getEmail());

        if (user.isEmpty() || !passwordEncoder
                .matches(loginDto.getPassword(), user.get().getPassword())) {
            throw new FailedAuthorizationException();
        }

        String accessToken = Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(jwtProperties.getLoginKey().getBytes()),
                        SignatureAlgorithm.HS512)
                .setHeaderParam("typ", TOKEN_TYPE)
                .setIssuer("rental-api")
                .setAudience("rental-app")
                .setSubject(user.get().getEmail())
                .setExpiration(new Date(System.currentTimeMillis() + jwtProperties.getLoginExpirationTimeMs()))
                .compact();

        return new TokenDto(accessToken);
    }

    @Override
    public void logout(String token) {
        Jws<Claims> parsedToken =
                Jwts.parserBuilder()
                        .setSigningKey(jwtProperties.getLoginKey().getBytes())
                        .build()
                        .parseClaimsJws(token);
        this.blacklist.add(parsedToken);
    }
}
