package com.papaj.rental.services;

import com.papaj.rental.dto.item.CreateItemDto;
import com.papaj.rental.dto.item.DeleteItemDto;
import com.papaj.rental.dto.item.ItemDto;
import com.papaj.rental.dto.item.ItemWithReservationsDto;
import com.papaj.rental.dto.reservation.CreateItemReservationDto;
import com.papaj.rental.dto.reservation.DeleteItemReservationDto;
import com.papaj.rental.models.Item;

import java.util.List;

public interface ItemService {

    Item getItemById(long id);

    long addItemToGroup(String loggedUserEmail, CreateItemDto createItemDto);

    void deleteItem(String loggedUserEmail, DeleteItemDto deleteItemDto);

    ItemWithReservationsDto addReservation(String loggedUserEmail, CreateItemReservationDto createItemReservationDto);

    ItemWithReservationsDto deleteReservation(String loggedUserEmail, DeleteItemReservationDto deleteItemReservationDto);

    ItemWithReservationsDto getItemWithReservations(long itemId);

    void updateItem(String loggedUserEmail, ItemDto updateItemDto);

    List<ItemDto> getItemsForGroup(String loggedUserEmail, long groupId);
}
