package com.papaj.rental.services.impl;

import com.papaj.rental.exceptions.NotFoundException;
import com.papaj.rental.exceptions.UnauthorizedException;
import com.papaj.rental.models.GroupMember;
import com.papaj.rental.models.ItemReservation;
import com.papaj.rental.models.UserRoleEnum;
import com.papaj.rental.repository.GroupMemberRepository;
import com.papaj.rental.repository.ItemReservationRepository;
import com.papaj.rental.services.UtilsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class UtilsServiceImpl implements UtilsService {

    private final GroupMemberRepository groupMemberRepository;
    private final ItemReservationRepository itemReservationRepository;

    public UtilsServiceImpl(
            GroupMemberRepository groupMemberRepository,
            ItemReservationRepository itemReservationRepository) {
        this.groupMemberRepository = groupMemberRepository;
        this.itemReservationRepository = itemReservationRepository;
    }

    @Override
    public boolean isUserAGroupAdmin(String userEmail, long groupId) {
        GroupMember checkingMember =
                this.groupMemberRepository
                        .findByUser_EmailAndGroup_Id(userEmail, groupId)
                        .orElseThrow(NotFoundException::new);
        return checkingMember.getUserRole() == UserRoleEnum.ADMIN;
    }

    @Override
    public boolean isUserAGroupAdminOrModerator(String userEmail, long groupId) {
        GroupMember checkingMember =
                this.groupMemberRepository
                        .findByUser_EmailAndGroup_Id(userEmail, groupId)
                        .orElseThrow(NotFoundException::new);
        return checkingMember.getUserRole() == UserRoleEnum.ADMIN
                || checkingMember.getUserRole() == UserRoleEnum.MODERATOR;
    }

    @Override
    public boolean isUserAllowedToReserveItemsInGroup(String userEmail, long groupId) {
        this.groupMemberRepository
                .findByUser_EmailAndGroup_Id(userEmail, groupId)
                .orElseThrow(NotFoundException::new);
        return true;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteReservationsFromList(ArrayList<Long> reservationsList, String loggedUserEmail) {
        for(Long id : reservationsList) {
            ItemReservation reservation = this.itemReservationRepository.findById(id).orElseThrow(NotFoundException::new);
            long groupId = reservation.getItem().getGroup().getId();

            if (!this.isUserAllowedToReserveItemsInGroup(loggedUserEmail, groupId)) {
                throw new UnauthorizedException();
            }
            this.itemReservationRepository.deleteById(id);
        }
    }
}
