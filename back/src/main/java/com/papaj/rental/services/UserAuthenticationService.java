package com.papaj.rental.services;


import com.papaj.rental.dto.LoginDto;
import com.papaj.rental.dto.TokenDto;

public interface UserAuthenticationService {

  TokenDto login(LoginDto loginDto);

    void logout(String token);
}
