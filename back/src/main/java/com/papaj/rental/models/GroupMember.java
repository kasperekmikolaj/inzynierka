package com.papaj.rental.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "group_member")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupMember extends DefaultEntity {

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    private Group group;

    @NotBlank
    @Size(max = 7)
    @Column(nullable = false, length = 7)
    private String userColor;

    @NotBlank
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20)
    private UserRoleEnum userRole;

    public GroupMember(User user, Group group, UserRoleEnum userRole) {
        this.user = user;
        this.group = group;
        this.userColor = "#ff0000";
        this.userRole = userRole;
    }
}
