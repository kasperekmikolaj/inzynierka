package com.papaj.rental.models;

public enum UserRoleEnum {

    ADMIN,
    MODERATOR,
    MEMBER
}
