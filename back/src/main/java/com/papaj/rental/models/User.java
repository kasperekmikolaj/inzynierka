package com.papaj.rental.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "\"user\"",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User extends DefaultEntity {

    @NotBlank
    @Size(min = 4, max = 25)
    @Column(nullable = false, unique = true, length = 25)
    private String username;

    @NotBlank
    @Size(min = 5, max = 255)
    @Email
    @Column(nullable = false, unique = true, length = 255)
    private String email;

    @NotBlank
    @Size(min = 8, max = 60)
    @Column(nullable = false, length = 60)
    private String password;

    @Size(max = 20)
    @Column(length = 20)
    private String telephone;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<GroupMember> groups;
}
