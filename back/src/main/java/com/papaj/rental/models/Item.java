package com.papaj.rental.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "item")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Item extends DefaultEntity {

    @NotBlank
    @Size(min = 4, max = 25)
    @Column(nullable = false, length = 25)
    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    private Group group;

    @OneToMany(mappedBy = "item", cascade = CascadeType.REMOVE)
    private List<ItemReservation> reservations;

    private String image;
}
