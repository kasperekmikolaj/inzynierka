package com.papaj.rental.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Entity
@Table(name = "item_reservation")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ItemReservation extends DefaultEntity {

    @NotBlank
    @Column(nullable = false)
    private LocalDate reservationDate;

    @ManyToOne
    private Item item;

    @ManyToOne
    @JoinColumn(name = "booking_user_id")
    private User bookingUser;
}
