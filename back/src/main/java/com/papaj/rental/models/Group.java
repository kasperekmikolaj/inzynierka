package com.papaj.rental.models;

import com.papaj.rental.dto.group.CreateGroupDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "\"group\"")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Group extends DefaultEntity {

    @NotBlank
    @Size(min = 4, max = 40)
    @Column(nullable = false, length = 40)
    private String name;

    @Size(max = 20)
    @Column(length = 20)
    private String type;

    private String image;

    private String description;

    @NotNull
    @Column(nullable = false)
    private Boolean isPublic;

    @OneToMany(mappedBy = "group", cascade = { CascadeType.REMOVE, CascadeType.PERSIST })
    private List<GroupMember> members;

    @OneToMany(mappedBy = "group", cascade = CascadeType.REMOVE)
    private List<Item> items;

    public Group(CreateGroupDto createGroupDto) {
        this.name = createGroupDto.getName();
        this.type = createGroupDto.getType();
        this.image = createGroupDto.getImage();
        this.description = createGroupDto.getDescription();
        this.isPublic = createGroupDto.isPublic();
        this.members = new ArrayList<>();
        this.items = new ArrayList<>();
    }
}
