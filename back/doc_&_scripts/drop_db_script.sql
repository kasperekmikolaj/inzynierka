BEGIN;
Drop table item_reservation;
Drop table item;
Drop table "group_invitation";
Drop table group_member;
Drop table "group";
Drop table "user";
Drop table "flyway_schema_history";
COMMIT;