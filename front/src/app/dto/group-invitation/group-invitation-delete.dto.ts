export class GroupInvitationDeleteDto {

  constructor(
    public invitedUserId: number,
    public groupId: number
  ) {
  }
}
