export class GroupMemberPatchColorDto {

  constructor(
    public userToChangeId: number,
    public groupId: number,
    public newColor: string
  ) {
  }
}
