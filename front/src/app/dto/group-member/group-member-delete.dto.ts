export class GroupMemberDeleteDto {

  constructor(
    public userId: number,
    public groupId: number
  ) {
  }
}
