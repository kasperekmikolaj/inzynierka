export class GroupMemberPatchRoleDto {

  constructor(
    public  userToChangeId: number,
    public  groupId: number,
    public  role: string,
  ) {
  }
}
