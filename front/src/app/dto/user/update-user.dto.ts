export class UpdateUserDto{

  constructor(
    public username: string = '',
    public email: string = '',
    public password: string = '',
    public telephone: string = ''
  ) {}
}
