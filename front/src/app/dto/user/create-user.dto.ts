export class CreateUserDto{

  public username: string;
  public email: string;
  public password: string;

  constructor(username: string, email: string, password: string) {
    this.username = username;
    this.password = password;
    this.email = email;
  }
}
