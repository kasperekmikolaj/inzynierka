export class BasicGroupDataDto {

  constructor(
    public name: string,
    public type: string,
    public image: string,
    public description: string,
    public isPublic: boolean,
  ) {}
}
