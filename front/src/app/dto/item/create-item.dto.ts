export class CreateItemDto {

  constructor(
    public name: string,
    public description: string,
    public image: string,
    public groupId: number
  ) {
  }
}
