export class DeleteItemDto {

  constructor(
    public itemId: number,
    public groupId: number
  ) {
  }
}
