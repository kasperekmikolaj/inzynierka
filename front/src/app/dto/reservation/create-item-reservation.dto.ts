export class CreateItemReservationDto {

  constructor(
    public itemId: number,
    public dates: string[]
  ) {
  }
}
