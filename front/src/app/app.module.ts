import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {authInterceptorProviders} from './_helpers/auth.interceptor';
import {AppRoutingModule} from './app-routing.module';
import {CircleAvatarComponent} from './components/circle-avatar/circle-avatar.component';
import {UserGroupsComponent} from './components/group/user-groups/user-groups.component';
import {GroupCardComponent} from './components/group/group-card/group-card.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {GroupDetailsComponent} from './components/group/group-details/group-details.component';
import {CalendarComponent} from './components/group/calendar/calendar.component';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {CalendarHeaderComponent} from './components/group/calendar/calendar-header/calendar-header.component';
import {ItemManagementComponent} from './components/item/item-management/item-management.component';
import { ItemFormComponent } from './components/item/item-management/item-form/item-form.component';
import { ItemDetailsComponent } from './components/item/item-management/item-details/item-details.component';
import { MembersManagementComponent } from './components/member/members-management/members-management.component';
import { InviteMemberComponent } from './components/member/members-management/invite-member/invite-member.component';
import { MemberInfoComponent } from './components/member/members-management/member-info/member-info.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { InvitationsListComponent } from './components/invitations-list/invitations-list.component';
import { ProfileComponent } from './components/profile/profile.component';
import { GroupFormComponent } from './components/group/group-form/group-form.component';
import { PublicGroupsComponent } from './components/group/public-groups/public-groups.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { Ng2SearchPipeModule } from "ng2-search-filter";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    CircleAvatarComponent,
    UserGroupsComponent,
    GroupCardComponent,
    GroupDetailsComponent,
    CalendarComponent,
    CalendarHeaderComponent,
    ItemManagementComponent,
    ItemFormComponent,
    ItemDetailsComponent,
    MembersManagementComponent,
    InviteMemberComponent,
    MemberInfoComponent,
    InvitationsListComponent,
    ProfileComponent,
    GroupFormComponent,
    PublicGroupsComponent,
    FooterComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    ColorPickerModule,
    Ng2SearchPipeModule,
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
