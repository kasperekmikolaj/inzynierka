import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ItemModel} from "../../../../models/item/item.model";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CreateItemDto} from "../../../../dto/item/create-item.dto";
import {ItemService} from "../../../../_services/item.service";
import {Router} from "@angular/router";
import {Const} from "../../../../constraints";

@Component({
    selector: 'app-item-form',
    templateUrl: './item-form.component.html',
    styleUrls: ['./item-form.component.css']
})
export class ItemFormComponent implements OnChanges {

    @Input() item: ItemModel;
    @Input() groupId: number;
    @Input() editing: boolean;
    @Output() editedEmitter: EventEmitter<void> = new EventEmitter<void>();
    image: string;
    itemForm: FormGroup;
    nameForm: FormControl;
    descriptionForm: FormControl;

    constructor(
        private itemService: ItemService,
    ) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.editing) {
            this.initFormAndPopulateWithData()
        } else {
            this.initEmptyForm()
        }
    }

    onFileSelected(event) {
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            let reader = new FileReader();
            let currentComponent = this;
            reader.readAsDataURL(file);
            reader.onload = function () {
                let result = reader.result;
                if (typeof result === 'string') {
                    currentComponent.image = <string>reader.result;
                } else {
                    currentComponent.image = String.fromCharCode.apply(null, new Uint16Array(<ArrayBuffer>reader.result));
                }
            };
        }
    }

    initFormAndPopulateWithData() {
        this.nameForm = new FormControl(this.item.name,
            [Validators.required, Validators.minLength(Const.MIN_ITEM_NAME_LEN), Validators.maxLength(Const.MAX_ITEM_NAME_LEN)]);
        this.descriptionForm = new FormControl(this.item.description);
        this.itemForm = new FormGroup({
            name: this.nameForm,
            description: this.descriptionForm,
            image: new FormControl(null),
        });
    }

    initEmptyForm() {
        this.nameForm = new FormControl('',
            [Validators.required, Validators.minLength(Const.MIN_ITEM_NAME_LEN), Validators.maxLength(Const.MAX_ITEM_NAME_LEN)]);
        this.descriptionForm = new FormControl('');
        this.itemForm = new FormGroup({
            name: this.nameForm,
            description: this.descriptionForm,
            image: new FormControl(null),
        });
    }

    onSubmit() {
        let values = this.itemForm.value;
        if (this.editing) {
            this.item.name = values.name;
            this.item.description = values.description;
            if (this.image != null) {
              this.item.image = this.image;
            }
            this.itemService.updateItem(this.item).subscribe(() => {
                this.editedEmitter.next();
            });
        } else {
            let newItem = new CreateItemDto(values.name, values.description, this.image, this.groupId);
            this.itemService.createNewItem(newItem).subscribe((newItemId: number) => {
                this.itemService.newItemIdSubject.next(newItemId);
                this.itemForm.reset();
                this.editedEmitter.next();
            });
        }
    }

}
