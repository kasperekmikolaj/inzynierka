import {Component, OnInit} from '@angular/core';
import {ItemModel} from "../../../models/item/item.model";
import {ItemService} from "../../../_services/item.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {DeleteItemDto} from "../../../dto/item/delete-item.dto";

@Component({
  selector: 'app-item-management',
  templateUrl: './item-management.component.html',
  styleUrls: ['./item-management.component.css']
})
export class ItemManagementComponent implements OnInit {

  groupId: number;
  selectedItem: ItemModel;
  items: ItemModel[] = [];
  formComponent: boolean = false;
  editing: boolean = false;
  isInfo: boolean = false;

  constructor(
    private itemService: ItemService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    if (this.router.url.indexOf('/items-info') > -1) {
      this.isInfo = true;
    }
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.groupId = +params['group_id'];
        this.itemService.getItemsForGroup(this.groupId).subscribe(
          items => {
            this.items = items;
            if (items.length > 0) {
              this.selectedItem = this.items[0];
            }
          });
      });

    this.itemService.newItemIdSubject.subscribe((newItemId: number) =>
      this.itemService.getItemsForGroup(this.groupId).subscribe(
        items => {
          this.items = items;
          if (items.length > 0) {
            this.selectedItem = this.items.find((item) => item.id == newItemId);
          }
        }));
  }

  onItemClicked(index: number) {
    this.selectedItem = this.items[index];
    this.formComponent = false;
  }

  onDeleteClicked(index: number) {
    if (confirm("Are you sure to delete item: \"" + this.items[index].name + "\"")) {
      let deleteDto = new DeleteItemDto(this.items[index].id, this.groupId);
      let deletedSelected: boolean = this.items[index].id == this.selectedItem.id;
      this.itemService.deleteItem(deleteDto).subscribe(() => {
        this.itemService.getItemsForGroup(this.groupId).subscribe(
          items => {
            this.items = items;
            if (deletedSelected && items.length > 0) {
              this.selectedItem = this.items[0];
            }
          });
      });
    }
  }

  onAddNewItemClicked() {
    this.formComponent = true;
    this.editing = false;
  }

  onUpdateItemClicked() {
    this.editing = true;
    this.formComponent = true;
  }

  onEditedItem() {
    this.editing = false;
    this.formComponent = false;
  }

}
