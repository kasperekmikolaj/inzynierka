import { Component, OnInit } from '@angular/core';
import {GroupModel} from "../../models/group/group.model";
import {GroupService} from "../../_services/group.service";
import {InvitationService} from "../../_services/invitation.service";

@Component({
  selector: 'app-invitations-list',
  templateUrl: './invitations-list.component.html',
  styleUrls: ['./invitations-list.component.css']
})
export class InvitationsListComponent implements OnInit {

  invitingGroups: GroupModel[] = [];

  constructor(
    private groupService: GroupService,
    private invitationService: InvitationService,
  ) { }

  ngOnInit(): void {
    this.groupService.getInvitingGroups()
      .subscribe((invitingGroups: GroupModel[]) => {
        this.invitingGroups = invitingGroups;
      });
    this.invitationService.invitationsUpdateSubject
      .subscribe(() => {
        this.groupService.getInvitingGroups()
          .subscribe((invitingGroups: GroupModel[]) => {
            this.invitingGroups = invitingGroups;
          });
      })
  }

}
