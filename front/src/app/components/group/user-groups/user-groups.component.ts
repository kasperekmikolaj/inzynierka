import { Component, OnInit } from '@angular/core';
import {GroupService} from "../../../_services/group.service";
import {GroupModel} from "../../../models/group/group.model";

@Component({
  selector: 'app-user-groups',
  templateUrl: './user-groups.component.html',
  styleUrls: ['./user-groups.component.css']
})
export class UserGroupsComponent implements OnInit {

  userGroups: GroupModel[] = [];

  constructor(
    private groupService: GroupService
  ) { }

  ngOnInit(): void {
    this.groupService.getAllUserGroups()
      .subscribe((groups: GroupModel[]) => this.userGroups = groups)
  }

}
