import {ItemWithReservationModel} from "../../../models/item/item-with-reservation.model";
import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges, ViewEncapsulation,} from '@angular/core';
import {CalendarEvent, CalendarMonthViewDay, CalendarView,} from 'angular-calendar';
import {ItemReservationModel} from "../../../models/reservation/item-reservation.model";
import {CalendarReservationWithEmail} from "../../../models/reservation/calendar-reservation-with-email";
import {LocalStorageService} from "../../../_services/local-storage.service";
import {ItemReservationService} from "../../../_services/item-reservation.service";
import {CreateItemReservationDto} from "../../../dto/reservation/create-item-reservation.dto";
import {DeleteItemReservationDto} from "../../../dto/reservation/delete-item-reservation.dto";

@Component({
  selector: 'app-callendar',
  templateUrl: './calendar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./calendar.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarComponent implements OnChanges {

  private static RESERVED_CLASS = "-reserved";
  private static SELECTED_CLASS = "cal-day-selected";

  @Input() item: ItemWithReservationModel;
  @Input() showForMember: boolean;
  @Input() userRole: string;
  view: CalendarView = CalendarView.Month;
  viewDate: Date = new Date();
  events: CalendarEvent[] = [];
  selectedMonthViewDay: CalendarMonthViewDay;
  selectedDays: CalendarMonthViewDay[] = [];
  currentlyShowingDays: CalendarMonthViewDay[] = [];
  reservedDays: CalendarReservationWithEmail[] = [];
  canCreateReservation = false;
  onlyDelete: boolean = false;

  constructor(
    private storageService: LocalStorageService,
    private itemReservationService: ItemReservationService,
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.clearSelectedDays();
    this.clearReservedDays();
    this.changePastDaysColors();
    this.item.reservations.forEach((reservation: ItemReservationModel) => reservation.date = new Date(reservation.date));
    this.changeReservedDaysColors();
  }

  beforeMonthViewRender({body}: { body: CalendarMonthViewDay[] }): void {
    this.currentlyShowingDays = body;
    this.currentlyShowingDays.forEach(day => day.cssClass = "");
    this.changePastDaysColors();
    this.changeSelectedDaysColor();
    this.changeReservedDaysColors();
  }

  changeSelectedDaysColor() {
    this.currentlyShowingDays.forEach((currentlyShowingDay) => {
      this.selectedDays.forEach(selectedDay => {
        if (selectedDay.date.toDateString() === currentlyShowingDay.date.toDateString()) {
          currentlyShowingDay.cssClass = CalendarComponent.SELECTED_CLASS;
        }
      })
    });
  }

  changeReservedDaysColors() {
    this.currentlyShowingDays.forEach((currentlyShowingDay) => {
      this.item.reservations.forEach(
        (reservation: ItemReservationModel) => {
          if (reservation.date.toDateString() === currentlyShowingDay.date.toDateString()) {
            this.reservedDays.push(new CalendarReservationWithEmail(currentlyShowingDay, reservation.bookingUserEmail));
            currentlyShowingDay.backgroundColor = reservation.color;
            currentlyShowingDay.cssClass =
              currentlyShowingDay.cssClass === CalendarComponent.SELECTED_CLASS ?
                currentlyShowingDay.cssClass + CalendarComponent.RESERVED_CLASS : CalendarComponent.RESERVED_CLASS;
          }
        })
    });
  }

  clearSelectedDays() {
    this.selectedDays.forEach(selectedDay => {
      this.currentlyShowingDays.forEach(currentlyShowingDay => {
        if (currentlyShowingDay.date.toDateString() === selectedDay.date.toDateString()) {
          currentlyShowingDay.cssClass = currentlyShowingDay.cssClass.replace(CalendarComponent.SELECTED_CLASS, "");
        }
      })
    })
    this.selectedDays = [];
  }

  clearReservedDays() {
    this.reservedDays.forEach(reservedDay => {
      this.currentlyShowingDays.forEach(currentlyShowingDay => {
        if (currentlyShowingDay.date.toDateString() === reservedDay.calendarDay.date.toDateString()) {
          currentlyShowingDay.backgroundColor = null;
          currentlyShowingDay.cssClass = currentlyShowingDay.cssClass.replace(CalendarComponent.RESERVED_CLASS, "");
        }
      })
    })
    this.reservedDays = [];
  }

  dayClicked(clickedDay: CalendarMonthViewDay): void {
    if (this.item.itemId == null) {
      return;
    }
    let isAllowedToSelect = this.isAllowedToSelect(clickedDay);
    if (!isAllowedToSelect) {
      return;
    }
    this.selectedMonthViewDay = clickedDay;
    const selectedDateTime = this.selectedMonthViewDay.date.toDateString();
    const dateIndex = this.selectedDays.findIndex(
      (selectedDay) => selectedDay.date.toDateString() === selectedDateTime
    );
    if (dateIndex > -1) {
      this.selectedMonthViewDay.cssClass = this.selectedMonthViewDay.cssClass.replace(CalendarComponent.SELECTED_CLASS, "");
      this.selectedDays.splice(dateIndex, 1);
    } else {
      this.selectedDays.push(this.selectedMonthViewDay);
      clickedDay.cssClass = CalendarComponent.SELECTED_CLASS + clickedDay.cssClass;
      this.selectedMonthViewDay = clickedDay;
    }

    this.onlyDelete = false;
    if (this.userRole === 'ADMIN') {
      this.reservedDays.forEach((reservedDay) => {
        this.selectedDays.forEach((selectedDay) => {
          if (reservedDay.calendarDay.date.toDateString() === selectedDay.date.toDateString()
            && reservedDay.bookingUserEmail !== this.storageService.getUserEmail()
          ) {
            this.onlyDelete = true;
          }
        });
      });
    }

    this.canCreateReservation = this.possibleToCreateReservation();
  }

  isAllowedToSelect(day: CalendarMonthViewDay): boolean {
    let isAllowed = true;
    if (day.isPast) {
      isAllowed = false;
    }
    this.reservedDays.forEach((reservedDay) => {
      if (reservedDay.calendarDay.date.toDateString() === day.date.toDateString()
        && reservedDay.bookingUserEmail !== this.storageService.getUserEmail()
      ) {
        if (this.userRole !== 'ADMIN') {
          isAllowed = false;
        }
      }
    });
    return isAllowed;
  }

  changePastDaysColors() {
    this.currentlyShowingDays.forEach((currentlyShowingDay) => {
      if (currentlyShowingDay.isPast) {
        currentlyShowingDay.backgroundColor = "#dbdbdb";
      }
    });
  }

  updateSelectedAfterAction(color: string, adding: boolean) {
    this.selectedDays.forEach(selectedDay => {
      this.currentlyShowingDays.forEach(currentlyShowingDay => {
        if (currentlyShowingDay.date.toDateString() === selectedDay.date.toDateString()) {
          currentlyShowingDay.backgroundColor = color;
          if (adding) {
            currentlyShowingDay.cssClass = CalendarComponent.RESERVED_CLASS;
          } else {
            currentlyShowingDay.cssClass = "";
          }
        }
      })
    })
    this.selectedDays = [];
  }

  possibleToCreateReservation(): boolean {
    let possible = true;

    if (this.selectedDays.length == 0) {
      return false;
    }

    this.selectedDays.forEach((selectedDay) => {
      if (
        this.reservedDays.findIndex(
          (reservedDay) =>
            selectedDay.date.toDateString() === reservedDay.calendarDay.date.toDateString()
        ) > -1) {
        possible = false;
        return;
      }
    });

    return possible;
  }

  onAddReservation() {
    let dates = [];
    this.selectedDays.forEach((day) => {
      let month = (day.date.getMonth() + 1) < 10 ? "0" + (day.date.getMonth() + 1).toString() : (day.date.getMonth() + 1).toString();
      let dayString = day.date.getDate() < 10 ? "0" + day.date.getDate().toString() : day.date.getDate().toString();
      dates.push(day.date.getFullYear().toString() + '-' + month + '-' + dayString);
    });
    let dto = new CreateItemReservationDto(this.item.itemId, dates);
    this.updateSelectedAfterAction("#00aaff", true);
    this.itemReservationService.addReservations(dto)
      .subscribe((item: ItemWithReservationModel) => {
        this.item = item;
        this.ngOnChanges(null);
      })
  }

  onDeleteReservation() {
    let reservationsIdList = [];
    this.selectedDays.forEach((selectedDate) => {
      let index = this.item.reservations.findIndex(
        (reservation) => reservation.date.toDateString() === selectedDate.date.toDateString());
      if (index > -1) {
        reservationsIdList.push(this.item.reservations[index].id);
      }
    });
    let deleteItemReservationDto = new DeleteItemReservationDto(reservationsIdList);
    this.updateSelectedAfterAction("#ffffff", false);
    this.itemReservationService.deleteReservations(deleteItemReservationDto).subscribe((item) => {
      this.item = item;
      this.ngOnChanges(null);
    });
  }

  onClearSelections() {
    this.clearSelectedDays();
    this.canCreateReservation = false;
    this.onlyDelete = false;
  }


}
