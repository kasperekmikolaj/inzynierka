import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Const} from "../../../constraints";
import {GroupService} from "../../../_services/group.service";
import {BasicGroupDataDto} from "../../../dto/group/basic-group-data.dto";
import {catchError} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {throwError} from "rxjs";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {GroupModel} from "../../../models/group/group.model";

@Component({
  selector: 'app-group-form',
  templateUrl: './group-form.component.html',
  styleUrls: ['./group-form.component.css']
})
export class GroupFormComponent implements OnInit {

  image: string;
  groupForm: FormGroup;
  nameForm: FormControl;
  typeForm: FormControl;
  descriptionForm: FormControl;
  isPublicForm: FormControl;
  needPopover: boolean = false;
  partOfDescription: string = '';
  errorOccurred: boolean = false;

  @ViewChild('imageInput') imageInput: ElementRef;
  editMode: boolean;
  groupId: number;
  group: GroupModel = new GroupModel();


  constructor(
    private groupService: GroupService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.nameForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_GROUP_NAME_LEN), Validators.maxLength(Const.MAX_GROUP_NAME_LEN)]);
    this.typeForm = new FormControl('',
      [Validators.maxLength(Const.MAX_TYPE_LEN)]);
    this.isPublicForm = new FormControl(false);
    this.descriptionForm = new FormControl('',);
    this.groupForm = new FormGroup({
      name: this.nameForm,
      description: this.descriptionForm,
      type: this.typeForm,
      isPublic: this.isPublicForm,
      image: new FormControl(null),
    });

    this.activatedRoute.params
      .subscribe((params: Params) => {
        if (params.id) {
          this.editMode = true;
          this.groupId = +params['id'];
          this.groupService.getGroupBasicData(this.groupId)
            .subscribe((groupModel: GroupModel) => {
              this.group = groupModel;
              this.populateFormWithGroupData();
            });
        } else {
          this.editMode = false;
        }
      });

    this.descriptionForm.valueChanges
      .subscribe((data) => {
        let description = this.descriptionForm.value;
        if (description.length > 60) {
          this.needPopover = true;
          let temp = description.substr(0, 60);
          let tempList = temp.split(" ");
          tempList[tempList.length - 1] = "";
          this.partOfDescription = tempList.join(" ") + "...";
        } else {
          this.needPopover = false;
        }
      });
  }

  onFileSelected(event) {
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      let reader = new FileReader();
      let currentComponent = this;
      reader.readAsDataURL(file);
      reader.onload = function () {
        let result = reader.result;
        if (typeof result === 'string') {
          currentComponent.image = <string>reader.result;
        } else {
          currentComponent.image = String.fromCharCode.apply(null, new Uint16Array(<ArrayBuffer>reader.result));
        }
      };
    }
  }

  onSubmit() {
    if (this.editMode) {
      let values = this.groupForm.value;
      let name = values.name == this.group.name ? null : values.name;
      let type = values.type == this.group.type ? null : values.type;
      let image = this.image == this.group.image ? null : this.image;
      let description = values.description == this.group.description ? null : values.description;
      let isPublic = values.isPublic == this.group.isPublic ? null : values.isPublic;
      let updatedGroupModel = new GroupModel(this.groupId, name, type, image, description, isPublic);
      this.groupService.updateGroup(updatedGroupModel)
        .subscribe(() => {
          this.router.navigate(["/user_groups"]);
        });
    } else {
      let values = this.groupForm.value;
      let createGroupDto = new BasicGroupDataDto(values.name, values.type, this.image, values.description, values.isPublic);
      this.groupService.createNewGroup(createGroupDto)
        .pipe(catchError((error: HttpErrorResponse) => {
          this.errorOccurred = true;
          return throwError("New group was not created.");
        }))
        .subscribe(() => {
          this.errorOccurred = false;
          this.router.navigate(["/user_groups"]);
        });
    }
  }

  populateFormWithGroupData() {
    this.imageInput.nativeElement.value = ""
    this.nameForm.setValue(this.group.name);
    this.descriptionForm.setValue(this.group.description);
    this.typeForm.setValue(this.group.type);
    this.isPublicForm.setValue(this.group.isPublic);
    this.image = this.group.image;
  }

  onDeleteButtonClicked() {
    if (confirm("Are you sure you want to delete this group?")) {
      this.groupService.deleteGroup(this.groupId)
        .subscribe(() => {
          this.router.navigate(["/user_groups"]);
        });
    }
  }

}
