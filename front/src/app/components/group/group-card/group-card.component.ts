import {Component, Input, OnInit} from '@angular/core';
import {GroupModel} from "../../../models/group/group.model";
import {Router} from "@angular/router";
import {InvitationService} from "../../../_services/invitation.service";

@Component({
  selector: 'app-group-card',
  templateUrl: './group-card.component.html',
  styleUrls: ['./group-card.component.css']
})
export class GroupCardComponent implements OnInit {

  @Input() group: GroupModel;
  @Input() isInvitation: boolean = false;
  partOfDescription: string;
  needPopover = false;

  constructor(
    private invitationService: InvitationService,
  ) { }

  ngOnInit(): void {
    let description = this.group.description;
    if (description.length > 60) {
      this.needPopover = true;
      let temp = description.substr(0, 60);
      let tempList = temp.split(" ");
      tempList[tempList.length-1] = "";
      this.partOfDescription = tempList.join(" ") + "...";
    }
  }

  onAcceptInvitationClicked() {
    this.invitationService.acceptInvitation(this.group.id);
  }

  onDeclineInvitationClicked() {
    this.invitationService.declineInvitation(this.group.id);
  }

}
