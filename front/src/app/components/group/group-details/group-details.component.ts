import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {GroupService} from "../../../_services/group.service";
import {GroupDetailsModel} from "../../../models/group/group-details.model";
import {ItemModel} from "../../../models/item/item.model";
import {GroupMemberModel} from "../../../models/group-member/group-member.model";
import {ItemWithReservationModel} from "../../../models/item/item-with-reservation.model";
import {ItemService} from "../../../_services/item.service";
import {MemberService} from "../../../_services/member.service";

@Component({
  selector: 'app-private-group',
  templateUrl: './group-details.component.html',
  styleUrls: ['./group-details.component.css']
})
export class GroupDetailsComponent implements OnInit {

  groupId: number;
  groupDetails: GroupDetailsModel = new GroupDetailsModel();
  items: ItemModel[] = [];
  members: GroupMemberModel[];
  selectedItem: ItemWithReservationModel = new ItemWithReservationModel();
  selectedItemId: number;
  noItems: boolean;
  userRole: string;
  showForMember: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private groupService: GroupService,
    private itemService: ItemService,
    private memberService: MemberService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.groupId = +params['id'];
        this.groupService.getGroupDetails(this.groupId)
          .subscribe((details: GroupDetailsModel) => {
            this.groupDetails = details;
            this.items = details.items;
            this.members = details.members;
            this.noItems = this.selectedItem == null;
            this.userRole = details.currentUserRole;
            this.selectedItemId = details.items[0] == null ? -1 : details.items[0].id
            this.showForMember = details.isUserGroupMember;
            if (this.selectedItemId !== -1) {
              this.itemService.getItemWithReservations(this.selectedItemId)
                .subscribe(item => {
                  this.selectedItem = item;
                })
            }
          });
      }
    )
  }

  onItemClicked(itemId: number) {
    this.itemService.getItemWithReservations(itemId)
      .subscribe(item => {
        this.selectedItem = item;
        this.selectedItemId = item.itemId;
      })
  }

  onLeaveGroupClicked() {
    if (confirm("Are you sure you want to leave this group?")) {
      this.memberService.leaveGroup(this.groupId).subscribe(() =>
        this.router.navigate(["/user_groups"])
      );
    }
  }

}
