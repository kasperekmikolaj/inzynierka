import { Component, OnInit } from '@angular/core';
import {GroupModel} from "../../../models/group/group.model";
import {GroupService} from "../../../_services/group.service";

@Component({
  selector: 'app-public-groups',
  templateUrl: './public-groups.component.html',
  styleUrls: ['./public-groups.component.css']
})
export class PublicGroupsComponent implements OnInit {

  publicGroups: GroupModel[] = [];
  searchText: string;

  constructor(
    private groupService: GroupService
  ) { }

  ngOnInit(): void {
    this.groupService.getPublicGroupList()
      .subscribe((groups: GroupModel[]) => this.publicGroups = groups);
  }

}
