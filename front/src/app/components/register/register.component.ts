import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../_services/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CreateUserDto} from '../../dto/user/create-user.dto';
import '../../constraints';
import {Router} from "@angular/router";
import {Const} from "../../constraints";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  usernameForm: FormControl;
  emailForm: FormControl;
  passwordForm: FormControl;
  passwordConfirmForm: FormControl;
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.usernameForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_USERNAME_LEN), Validators.maxLength(Const.MAX_USERNAME_LEN)]);
    this.emailForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_EMAIL_LEN), Validators.maxLength(Const.MAX_EMAIL_LEN), Validators.email]);
    this.passwordForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_PASSWORD_LEN), Validators.maxLength(Const.MAX_PASSWORD_LEN)]);
    this.passwordConfirmForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_PASSWORD_LEN), Validators.maxLength(Const.MAX_PASSWORD_LEN),
      ]);

    this.registerForm = new FormGroup({
      username: this.usernameForm,
      email: this.emailForm,
      password: this.passwordForm,
      password_confirm: this.passwordConfirmForm,
    });
  }

  onSubmit(): void {
    const values = this.registerForm.value;
    const user = new CreateUserDto(values.username, values.email, values.password);
    this.authService.register(user).subscribe(
      data => {
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.router.navigate(['login']);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

}
