import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../_services/auth.service';
import {LocalStorageService} from '../../_services/local-storage.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Const} from '../../constraints';
import {LoginDto} from '../../dto/login.dto';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  emailForm: FormControl;
  passwordForm: FormControl;
  errorMessage = '';
  isLoginFailed = false;

  constructor(private authService: AuthService, private tokenStorage: LocalStorageService, private router: Router) {
  }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      //todo
      // already logged in -> home redirect
    }
    this.emailForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_EMAIL_LEN), Validators.maxLength(Const.MAX_EMAIL_LEN), Validators.email]);
    this.passwordForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_PASSWORD_LEN), Validators.maxLength(Const.MAX_PASSWORD_LEN)]);

    this.loginForm = new FormGroup({
      email: this.emailForm,
      password: this.passwordForm,
    });
  }

  onSubmit(): void {
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;
    const loginDto = new LoginDto(email, password);
    this.authService.login(loginDto).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.isLoginFailed = false;

        this.authService.loginSubject.next(true);
        this.router.navigate(['']);
      },
      err => {
        this.isLoginFailed = true;
      }
    );
  }
}
