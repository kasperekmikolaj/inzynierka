import {Component, OnInit, ViewChild, ElementRef, AfterViewInit, AfterViewChecked} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {MemberService} from "../../../../_services/member.service";
import {GroupMemberModel} from "../../../../models/group-member/group-member.model";
import {GroupMemberPatchColorDto} from "../../../../dto/group-member/group-member-patch-color.dto";
import {UserRoleEnumService} from "../../../../_services/user-role-enum.service";
import {GroupMemberPatchRoleDto} from "../../../../dto/group-member/group-member-patch-role.dto";
import {LocalStorageService} from "../../../../_services/local-storage.service";

@Component({
  selector: 'app-member-info',
  templateUrl: './member-info.component.html',
  styleUrls: ['./member-info.component.css']
})
export class MemberInfoComponent implements OnInit, AfterViewInit {

  START_OPTION = "Select new role for member.";

  @ViewChild('rectangle') rectangle: ElementRef;
  groupId: number;
  member: GroupMemberModel = new GroupMemberModel();
  currentUserEmail: string;
  userRoles: string[] = [];
  selectedColor: string;
  selectedRole: string = this.START_OPTION;
  changeColor: boolean = false;
  changeRole: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private memberService: MemberService,
    private userRoleEnumService: UserRoleEnumService,
    private localStorageService: LocalStorageService,
  ) {}

  ngOnInit(): void {
    if (this.memberService.lastSelectedMember != null) {
      this.member = this.memberService.lastSelectedMember;
      this.selectedColor = this.member.userColor;
    }
    this.currentUserEmail = this.localStorageService.getUserEmail();
    this.activatedRoute.parent.params.subscribe(
      (params: Params) => {
        this.groupId = +params['group_id'];
      });
    this.memberService.memberSelectedSubject
      .subscribe((selectedMember: GroupMemberModel) => {
        this.changeColor = false;
        this.changeRole = false;
        this.member = selectedMember;
        this.rectangle.nativeElement.style.fill = this.member.userColor;
        this.selectedColor = this.member.userColor;
      });
    this.userRoleEnumService.getUserRoles()
      .subscribe((roles: string[]) => {
        this.userRoles = roles;
      });
  }

  public onColorPickerClosed(event: string, data: any): void {
    this.selectedColor = data;
  }

  onUpdateColorClicked() {
    let groupMemberPatchColorDto = new GroupMemberPatchColorDto(this.member.userId, this.groupId, this.selectedColor);
    this.memberService.updateMemberColor(groupMemberPatchColorDto)
      .subscribe(
        () => {
          this.memberService.memberUpdatedSubject.next();
          this.changeColor = false;
        }
      )
  }

  onUpdateRoleClicked() {
    let newRoleDto = new GroupMemberPatchRoleDto(this.member.userId, this.groupId, this.selectedRole);
    this.memberService.updateMemberRole(newRoleDto)
      .subscribe(() => {
        this.memberService.memberUpdatedSubject.next();
        this.changeRole = false;
      });
  }

  ngAfterViewInit(): void {
    if (this.memberService.lastSelectedMember != null) {
      this.rectangle.nativeElement.style.fill = this.member.userColor;
    }
  }

}
