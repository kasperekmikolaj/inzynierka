import {AfterViewInit, Component, OnInit, ViewChildren} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {MemberService} from "../../../_services/member.service";
import {GroupMemberModel} from "../../../models/group-member/group-member.model";
import {UserModel} from "../../../models/user/user.model";
import {InvitationService} from 'src/app/_services/invitation.service';
import {GroupMemberDeleteDto} from "../../../dto/group-member/group-member-delete.dto";
import {GroupInvitationDeleteDto} from "../../../dto/group-invitation/group-invitation-delete.dto";

@Component({
  selector: 'app-members-management',
  templateUrl: './members-management.component.html',
  styleUrls: ['./members-management.component.css']
})
export class MembersManagementComponent implements OnInit {

  @ViewChildren('member_buttons') memberButtons;
  groupId: number;
  selectedMember: GroupMemberModel;
  members: GroupMemberModel[] = [];
  invitedUsers: UserModel[] = [];

  constructor(
    private memberService: MemberService,
    private activatedRoute: ActivatedRoute,
    private invitationService: InvitationService,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.groupId = +params['group_id'];
        this.memberService.getGroupMembers(this.groupId).subscribe(
          (members: GroupMemberModel[]) => {
            this.members = members;
            if (members.length > 0) {
              this.selectedMember = this.members[0];
              this.memberService.memberSelectedSubject.next(this.selectedMember);
            }
          })
        this.memberService.getUsersInvitedToGroup(this.groupId).subscribe(
          (invitedUsers: UserModel[]) => {
            this.invitedUsers = invitedUsers;
          })
      });
    this.invitationService.invitationsUpdateSubject.subscribe(
      () => {
        this.memberService.getUsersInvitedToGroup(this.groupId).subscribe(
          (invitedUsers: UserModel[]) => {
            this.invitedUsers = invitedUsers;
          });
      });
    this.memberService.memberUpdatedSubject.subscribe(
      () => {
        this.memberService.getGroupMembers(this.groupId).subscribe(
          (members: GroupMemberModel[]) => {
            this.members = members;
            this.selectedMember = members.find((member) => member.userId == this.selectedMember.userId);
            this.memberService.memberSelectedSubject.next(this.selectedMember);
          })
      });
  }

  onMemberClicked(index: number) {
    this.selectedMember = this.members[index];
    this.memberService.memberSelectedSubject.next(this.selectedMember);
    this.memberService.lastSelectedMember = this.selectedMember;
  }

  onDeleteMemberClicked(index: number) {
    if (confirm("Are you sure to delete member: \"" + this.members[index].username + "\"")) {
      let deleteGroupMemberDto = new GroupMemberDeleteDto(this.members[index].userId, this.groupId);
      this.memberService.deleteGroupMember(deleteGroupMemberDto).subscribe(
        () => {
          this.memberService.getGroupMembers(this.groupId).subscribe(
            (updatedMembers: GroupMemberModel[]) => {
              this.members = updatedMembers;
            });
        }
      )
    }
  }

  onDeleteInvitationClicked(index: number) {
    if (confirm("Are you sure to delete invitation for member: \"" + this.invitedUsers[index].username + "\"")) {
      let deleteGroupInvitationDto = new GroupInvitationDeleteDto(this.invitedUsers[index].userId, this.groupId);
      this.invitationService.deleteGroupInvitation(deleteGroupInvitationDto)
        .subscribe(() => {
          this.invitationService.invitationsUpdateSubject.next();
        })
    }
  }

}
