import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {InvitationService} from "../../../../_services/invitation.service";
import {GroupInvitationModel} from "../../../../models/group-invitation/group-invitation.model";
import {ActivatedRoute, Params} from "@angular/router";
import {GroupMemberModel} from "../../../../models/group-member/group-member.model";
import {UserModel} from "../../../../models/user/user.model";
import {catchError} from "rxjs/operators";
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-member',
  templateUrl: './invite-member.component.html',
  styleUrls: ['./invite-member.component.css']
})
export class InviteMemberComponent implements OnInit {

  emailForm: FormGroup;
  groupId: number;
  errorOccurred: boolean = false;

  constructor(
    private invitationService: InvitationService,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.parent.params.subscribe(
      (params: Params) => {
        this.groupId = +params['group_id'];
      });

    this.emailForm = new FormGroup({
      userEmail: new FormControl('', [Validators.required, Validators.email])
    });
  }

  onSubmit() {
    let newMemberEmail = this.emailForm.value.userEmail
    let groupInvitation = new GroupInvitationModel(newMemberEmail, this.groupId);
    this.invitationService.createGroupInvitation(groupInvitation)
      .pipe(catchError((error: HttpErrorResponse) => {
        this.errorOccurred = true;
        return throwError("New member was not invited.");
      }))
      .subscribe(
        () => {
          this.errorOccurred = false;
          this.invitationService.invitationsUpdateSubject.next();
        }
      );
    this.emailForm.reset();
  }

}
