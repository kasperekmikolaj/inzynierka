import {Component, OnInit} from '@angular/core';
import {UserService} from "../../_services/user.service";
import {UserModel} from "../../models/user/user.model";
import {AuthService} from "../../_services/auth.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Const} from "../../constraints";
import {UpdateUserDto} from "../../dto/user/update-user.dto";
import {catchError} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {throwError} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  phonePattern = "^[+]?[0-9-\\s]{6,18}$";
  user: UserModel = new UserModel();
  editing: boolean = false;
  deleteAccount: boolean = false;
  userFormGroup: FormGroup;
  emailForm: FormControl;
  usernameForm: FormControl;
  telephoneForm: FormControl;
  passwordForm: FormControl;
  passwordConfirmForm: FormControl;
  editEmail = false;
  editUsername = false;
  editTelephone = false;
  editPassword = false;
  updateFailed = false;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.initEditForm();
    this.userService.getUserData()
      .subscribe((userData: UserModel) => {
        this.user = userData;
        this.setDefaultFormValues();
      });
  }

  setDefaultFormValues() {
    this.userFormGroup.setValue({
      email: this.user.email,
      username: this.user.username,
      telephone: this.user.telephone,
      password: '',
      password_confirm: '',
    });
  }

  initEditForm() {
    this.emailForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_EMAIL_LEN), Validators.maxLength(Const.MAX_EMAIL_LEN), Validators.email]);
    this.usernameForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_USERNAME_LEN), Validators.maxLength(Const.MAX_USERNAME_LEN)]);
    this.telephoneForm = new FormControl('', Validators.pattern(this.phonePattern));
    this.passwordForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_PASSWORD_LEN), Validators.maxLength(Const.MAX_PASSWORD_LEN)]);
    this.passwordConfirmForm = new FormControl('',
      [Validators.required, Validators.minLength(Const.MIN_PASSWORD_LEN), Validators.maxLength(Const.MAX_PASSWORD_LEN),
      ]);

    this.userFormGroup = new FormGroup({
      email: this.emailForm,
      username: this.usernameForm,
      telephone: this.telephoneForm,
      password: this.passwordForm,
      password_confirm: this.passwordConfirmForm,
    });
  }

  onDeleteProfileClicked() {
    this.deleteAccount = true;
  }

  onDeleteProfileConfirmedClicked() {
    if (confirm("You will be removed from every group and all your reservations will be deleted. Are you sure you want to continue?")) {
      this.userService.deleteUser()
        .subscribe(() => {
          this.authService.logout();
        });
    } else {
      this.deleteAccount = false;
    }
  }

  onUpdateProfileClicked() {
    this.editing = true;
  }

  onCancelEditingClicked() {
    this.editEmail = false;
    this.editUsername = false;
    this.editTelephone = false;
    this.editPassword = false;
    this.editing = false;
    this.userFormGroup.reset();
    this.setDefaultFormValues();
  }

  onEmailEditClicked() {
    if (this.editEmail) {
      this.emailForm.setValue(this.user.email);
    }
    this.editEmail = !this.editEmail;
  }

  onUsernameEditClicked() {
    if (this.editUsername) {
      this.usernameForm.setValue(this.user.username);
    }
    this.editUsername = !this.editUsername;
  }

  onTelephoneEditClicked() {
    if (this.editTelephone) {
      this.telephoneForm.setValue(this.user.telephone);
    }
    this.editTelephone = !this.editTelephone;
  }

  PasswordEditClicked() {
    if (this.editPassword) {
      this.passwordForm.reset();
      this.passwordForm.setValue('');
      this.passwordConfirmForm.setValue('');
    }
    this.editPassword = !this.editPassword;
  }

  onFormSubmit() {
    if (confirm("You will need to re login after this operation. Do you want to proceed?")) {
      let email = this.emailForm.value == this.user.email ? null : this.emailForm.value;
      let username = this.usernameForm.value == this.user.username ? null : this.usernameForm.value;
      let telephone = this.telephoneForm.value == this.user.telephone ? null : this.telephoneForm.value;
      let password = this.passwordForm.value == '' ? null : this.passwordForm.value;

      let updateUserDto = new UpdateUserDto(username, email, password, telephone);
      this.userService.updateUser(updateUserDto)
        .pipe(catchError((error: HttpErrorResponse) => {
          this.updateFailed = true;
          return throwError("User profile update failed.");
        }))
        .subscribe(() => {
          this.authService.logout();
          this.router.navigate(["/login"]);
        });
    }
  }

}
