import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-circle-avatar',
  templateUrl: './circle-avatar.component.html',
  styleUrls: ['./circle-avatar.component.css']
})
export class CircleAvatarComponent implements OnInit {

  @Input() imagePatch: string;

  constructor() { }

  ngOnInit(): void {
  }

}
