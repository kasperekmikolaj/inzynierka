export class Const {
  public static MIN_USERNAME_LEN = 4;
  public static MAX_USERNAME_LEN = 25;

  public static MIN_EMAIL_LEN = 5;
  public static MAX_EMAIL_LEN = 255;

  public static MIN_PASSWORD_LEN = 8;
  public static MAX_PASSWORD_LEN = 120;

  public static APP_NAME = "ReserveThis";

  public static MAX_GROUP_NAME_LEN = 40;
  public static MIN_GROUP_NAME_LEN = 4;

  public static MAX_TYPE_LEN = 20;

  public static MAX_ITEM_NAME_LEN = 20;
  public static MIN_ITEM_NAME_LEN = 4;
}


