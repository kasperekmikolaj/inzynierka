import {GroupMemberModel} from "../group-member/group-member.model";
import {ItemModel} from "../item/item.model";

export class GroupDetailsModel {

  constructor(
    public id: number = null,
    public name: string = "",
    public type: string = "",
    public image: string = "",
    public description: string = "",
    public isPublic: boolean = false,
    public currentUserRole: string = "",
    public currentUserColor: string = "",
    public items: ItemModel[] = [],
    public members: GroupMemberModel[] = [],

    public isUserGroupMember: boolean = false
  ) {
  }
}
