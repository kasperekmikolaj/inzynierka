export class GroupModel {

  constructor(
    public id: number = null,
    public name: string = "",
    public type: string = "",
    public image: string = "",
    public description: string = "",
    public isPublic: boolean = false) {
  }
}
