export class UserModel {

  constructor(
    public username: string = '',
    public email: string = '',
    public userId: number = null,
    public telephone: string = '',
  ) {
  }
}
