export class GroupMemberModel {

  constructor(
    public userId: number = null,
    public username: string = "",
    public userColor: string = "",
    public userRole: string = "",
    public userEmail: string = ""
  ) {
  }
}
