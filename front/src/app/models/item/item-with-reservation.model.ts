import {ItemReservationModel} from "../reservation/item-reservation.model";

export class ItemWithReservationModel {

  constructor(
    public itemId: number = null,
    public name: string = "",
    public description: string = "",
    public image: string = "",
    public reservations: ItemReservationModel[] = []
  ) {
  }
}
