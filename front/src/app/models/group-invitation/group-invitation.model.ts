export class GroupInvitationModel {

  constructor(
    public invitedUserEmail: string,
    public groupId: number
  ) {
  }
}
