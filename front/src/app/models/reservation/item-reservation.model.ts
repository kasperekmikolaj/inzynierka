export class ItemReservationModel {

  constructor(
    public id: number,
    public date: Date,
    public color: string = "",
    public bookingUserEmail: string,
  ) {
  }
}
