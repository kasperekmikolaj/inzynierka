import {CalendarMonthViewDay} from "angular-calendar";

export class CalendarReservationWithEmail {

  constructor(
    public calendarDay: CalendarMonthViewDay,
    public bookingUserEmail: string,
  ) {
  }

}
