import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import {UserGroupsComponent} from './components/group/user-groups/user-groups.component';
import {GroupDetailsComponent} from './components/group/group-details/group-details.component';
import {ItemManagementComponent} from "./components/item/item-management/item-management.component";
import {ItemDetailsComponent} from "./components/item/item-management/item-details/item-details.component";
import {ItemFormComponent} from "./components/item/item-management/item-form/item-form.component";
import {MembersManagementComponent} from "./components/member/members-management/members-management.component";
import {InviteMemberComponent} from "./components/member/members-management/invite-member/invite-member.component";
import {MemberInfoComponent} from "./components/member/members-management/member-info/member-info.component";
import {InvitationsListComponent} from "./components/invitations-list/invitations-list.component";
import {ProfileComponent} from "./components/profile/profile.component";
import {GroupFormComponent} from "./components/group/group-form/group-form.component";
import {PublicGroupsComponent} from "./components/group/public-groups/public-groups.component";
import {HomeComponent} from "./components/home/home.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: HomeComponent},
  {path: 'user_groups', component: UserGroupsComponent},
  {path: 'public_groups', component: PublicGroupsComponent},
  {path: 'user_invitations', component: InvitationsListComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'new/group', component: GroupFormComponent},
  {path: 'group/:id', component: GroupDetailsComponent, pathMatch: 'full'},
  {path: 'group/:id/update', component: GroupFormComponent},
  {path: 'items-info/:group_id', component: ItemManagementComponent},
  {path: 'items/:group_id', component: ItemManagementComponent},
  {
    path: 'members/:group_id', component: MembersManagementComponent, children: [
      {path: 'new', component: InviteMemberComponent},
      {path: 'info', component: MemberInfoComponent},
    ]
  },
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
