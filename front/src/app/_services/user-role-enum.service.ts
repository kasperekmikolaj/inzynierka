import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {Observable} from "rxjs";
import {BACKEND} from "./c";

@Injectable({
  providedIn: 'root'
})
export class UserRoleEnumService {

  constructor(
    private http: HttpClient,
  ) {}

  getUserRoles(): Observable<string[]>{
    return this.http.get<string[]>(BACKEND + "user_role");
  }

}
