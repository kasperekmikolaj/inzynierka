import {Injectable} from '@angular/core';
import * as jwt_decode from 'jwt-decode';

const TOKEN_KEY = 'auth-token';
const USER_EMAIL_KEY = 'auth-user-email';
const EXPIRY_TIME = 'token-expiry-date';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() {
  }

  public saveToken(token: string): void {
    const decoded = jwt_decode(token);
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);

    const expiryTime = decoded.exp;
    window.localStorage.removeItem(EXPIRY_TIME);
    window.localStorage.setItem(EXPIRY_TIME, expiryTime);

    const email = decoded.sub;
    window.localStorage.removeItem(USER_EMAIL_KEY);
    window.localStorage.setItem(USER_EMAIL_KEY, email);
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  public getUserEmail(): string {
    return localStorage.getItem(USER_EMAIL_KEY);
  }

  public getExpiryDate(): Date {
    return new Date(localStorage.getItem(EXPIRY_TIME));
  }

  signOut(): void {
    window.localStorage.clear();
  }
}
