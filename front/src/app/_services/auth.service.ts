import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {LoginDto} from '../dto/login.dto';
import {CreateUserDto} from '../dto/user/create-user.dto';
import {LocalStorageService} from "./local-storage.service";

const AUTH_API = 'http://localhost:8080/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loginSubject: Subject<boolean> = new Subject<boolean>();

  constructor(private http: HttpClient, private localStorage: LocalStorageService) { }

  login(credentials: LoginDto): Observable<any> {
    return this.http.post(AUTH_API + 'login', credentials, httpOptions);
  }

  register(user: CreateUserDto): Observable<any> {
    return this.http.post(AUTH_API + 'signUp', user, httpOptions);
  }

  logout() {
    this.http.post(AUTH_API + "logout", null).subscribe();
    this.localStorage.signOut();
    this.loginSubject.next(false);
  }

  isLoggedIn(): boolean {
    const email = this.localStorage.getUserEmail();
    return email && !this.isTokenExpired();
  }

  isTokenExpired(): boolean {
    return new Date() > this.localStorage.getExpiryDate();
  }
}
