import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {GroupModel} from "../models/group/group.model";
import {BACKEND} from "./c";
import {Observable} from "rxjs";
import {GroupDetailsModel} from "../models/group/group-details.model";
import {BasicGroupDataDto} from "../dto/group/basic-group-data.dto";

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(
    private http: HttpClient
  ) {}

  public getAllUserGroups(): Observable<GroupModel[]> {
    return this.http.get<GroupModel[]>(BACKEND + "group/all_user");
  }

  public getGroupDetails(id: number): Observable<GroupDetailsModel> {
    return this.http.get<GroupDetailsModel>(BACKEND + "group/details", {params: {id: id.toString()} });
  }

  public getInvitingGroups(): Observable<GroupModel[]> {
    return this.http.get<GroupModel[]>(BACKEND + "group/invitations");
  }

  public createNewGroup(createGroupDto: BasicGroupDataDto): Observable<void> {
    return this.http.post<void>(BACKEND + "group", createGroupDto);
  }

  public getGroupBasicData(groupId: number): Observable<GroupModel> {
    return this.http.get<GroupModel>(BACKEND + "group/basic",{params: {id: groupId.toString()} });
  }

  public updateGroup(updatedGroupModel: GroupModel): Observable<void> {
    return this.http.patch<void>(BACKEND + "group", updatedGroupModel);
  }

  public deleteGroup(groupId: number): Observable<void> {
    return this.http.delete<void>(BACKEND + "group",{params: {id: groupId.toString()} });
  }

  public getPublicGroupList(): Observable<GroupModel[]> {
    return this.http.get<GroupModel[]>(BACKEND + "group/public");
  }

}
