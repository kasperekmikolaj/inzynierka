import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {GroupInvitationModel} from "../models/group-invitation/group-invitation.model";
import {BACKEND} from "./c";
import {Observable, Subject} from "rxjs";
import {GroupMemberDeleteDto} from "../dto/group-member/group-member-delete.dto";
import {GroupInvitationDeleteDto} from "../dto/group-invitation/group-invitation-delete.dto";

@Injectable({
  providedIn: 'root'
})
export class InvitationService {

  invitationsUpdateSubject: Subject<void> = new Subject<void>();

  constructor(
    private http: HttpClient
  ) {}

  createGroupInvitation(groupInvitation: GroupInvitationModel): Observable<void> {
    return this.http.post<void>(BACKEND + "invitation", groupInvitation);
  }

  deleteGroupInvitation(groupInvitationModel: GroupInvitationDeleteDto): Observable<void> {
    return this.http.post<void>(BACKEND + "invitation/delete", groupInvitationModel);
  }

  acceptInvitation(groupId: number): void {
    this.http.post<void>(BACKEND + "invitation/accept", null,{ params: {groupId: groupId.toString()} })
      .subscribe(() => this.invitationsUpdateSubject.next());
  }

  declineInvitation(groupId: number): void {
    this.http.post<void>(BACKEND + "invitation/decline", null, { params: {groupId: groupId.toString()} })
      .subscribe(() => this.invitationsUpdateSubject.next());
  }

}
