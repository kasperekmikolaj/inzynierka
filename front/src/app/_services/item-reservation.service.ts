import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {BACKEND} from "./c";
import {CreateItemReservationDto} from "../dto/reservation/create-item-reservation.dto";
import {ItemWithReservationModel} from "../models/item/item-with-reservation.model";
import {Injectable} from "@angular/core";
import {DeleteItemReservationDto} from "../dto/reservation/delete-item-reservation.dto";

@Injectable({
  providedIn: 'root'
})
export class ItemReservationService {

  constructor(
    private http: HttpClient
  ) {}

  public addReservations(createItemReservationDto: CreateItemReservationDto): Observable<ItemWithReservationModel> {
    return this.http.post<ItemWithReservationModel>(BACKEND + "item_reservation", createItemReservationDto);
  }

  public deleteReservations(deleteItemReservationDto: DeleteItemReservationDto): Observable<ItemWithReservationModel> {
    return this.http.post<ItemWithReservationModel>(BACKEND + "item_reservation/delete", deleteItemReservationDto);
  }

}
