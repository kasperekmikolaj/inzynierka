import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {BACKEND} from "./c";
import {ItemWithReservationModel} from "../models/item/item-with-reservation.model";
import {ItemModel} from "../models/item/item.model";
import {CreateItemDto} from "../dto/item/create-item.dto";
import {DeleteItemDto} from "../dto/item/delete-item.dto";

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  newItemIdSubject: Subject<number> = new Subject<number>();

  constructor(
    private http: HttpClient
  ) {}

  public getItemWithReservations(itemId: number): Observable<ItemWithReservationModel> {
    return this.http.get<ItemWithReservationModel>(BACKEND + "item/with_reservations", { params: {itemId: itemId.toString()} });
  }

  public getItemsForGroup(groupId: number): Observable<ItemModel[]> {
    return this.http.get<ItemModel[]>(BACKEND + "item/group", { params: {groupId: groupId.toString()} });
  }

  public createNewItem(createItemDto: CreateItemDto): Observable<number> {
    return this.http.post<number>(BACKEND + "item", createItemDto);
  }

  public deleteItem(deleteItemDto: DeleteItemDto): Observable<void> {
    return this.http.post<void>(BACKEND + "item/delete", deleteItemDto);
  }

  public updateItem(item: ItemModel): Observable<void> {
    return this.http.patch<void>(BACKEND + "item", item);
  }
}
