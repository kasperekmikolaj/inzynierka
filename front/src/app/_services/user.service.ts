import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {BACKEND} from "./c";
import {UserModel} from "../models/user/user.model";
import {UpdateUserDto} from "../dto/user/update-user.dto";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) {
  }

  public getUserData(): Observable<UserModel> {
    return this.http.get<UserModel>(BACKEND + "user");
  }

  public deleteUser(): Observable<void> {
    return this.http.delete<void>(BACKEND + "user");
  }

  public updateUser(updateUserDto: UpdateUserDto): Observable<void> {
    return this.http.patch<void>(BACKEND + "user", updateUserDto);
  }

}
