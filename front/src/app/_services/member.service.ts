import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {GroupMemberModel} from "../models/group-member/group-member.model";
import {BACKEND} from "./c";
import {Observable, Subject} from "rxjs";
import {UserModel} from "../models/user/user.model";
import {GroupMemberDeleteDto} from "../dto/group-member/group-member-delete.dto";
import {GroupMemberPatchColorDto} from "../dto/group-member/group-member-patch-color.dto";
import {GroupMemberPatchRoleDto} from "../dto/group-member/group-member-patch-role.dto";

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  memberSelectedSubject: Subject<GroupMemberModel> = new Subject<GroupMemberModel>();
  memberUpdatedSubject: Subject<void> = new Subject<void>();
  lastSelectedMember: GroupMemberModel;

  constructor(
    private http: HttpClient
  ) {}

  public getGroupMembers(groupId: number): Observable<GroupMemberModel[]> {
    return this.http.get<GroupMemberModel[]>(BACKEND + "group_member/all", {params: {groupId: groupId.toString()}})
  }

  public getUsersInvitedToGroup(groupId: number): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(BACKEND + "user/invitation", {params: {groupId: groupId.toString()}});
  }

  public deleteGroupMember(groupMemberDeleteDto: GroupMemberDeleteDto): Observable<void> {
    return this.http.post<void>(BACKEND + "group_member/delete", groupMemberDeleteDto);
  }

  public updateMemberColor(groupMemberPatchColorDto: GroupMemberPatchColorDto): Observable<void> {
    return this.http.patch<void>(BACKEND + "group_member/color", groupMemberPatchColorDto);
  }

  public updateMemberRole(groupMemberPatchRoleDto: GroupMemberPatchRoleDto): Observable<void> {
    return this.http.patch<void>(BACKEND + "group_member/role", groupMemberPatchRoleDto);
  }

  public leaveGroup(groupId: number): Observable<void> {
    return this.http.delete<void>(BACKEND + "group_member/leave_group", {params: {groupId: groupId.toString()}});
  }
}
