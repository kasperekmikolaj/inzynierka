import {Component, OnInit} from '@angular/core';
import {Const} from './constraints';
import {AuthService} from "./_services/auth.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  isLoggedIn = false;
  appName = Const.APP_NAME;

  constructor(
    private authService: AuthService,
    public appRouter: Router,
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();
    this.authService.loginSubject.subscribe(
      (isLoggedIn: boolean) => this.isLoggedIn = isLoggedIn);
  }

  logout(): void {
    this.authService.logout();
    window.location.reload();
  }
}
